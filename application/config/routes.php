<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = 'Authantication/adminIndex';
$route['dashboard'] = 'admin/dashboard';
$route['users'] = 'admin/user';
$route['school'] = 'admin/school';
$route['edit-school/(:any)'] = 'admin/school/edit/$1';

$route['language'] = 'admin/language';
$route['edit-language/(:any)'] = 'admin/language/edit/$1';
$route['slider'] = 'admin/slider';
$route['edit-slider/(:any)'] = 'admin/slider/edit/$1';


$route['class'] = 'admin/school_class';
$route['item'] = 'admin/class_item';
$route['book-set'] = 'admin/book_set';
$route['edit-school-class/(:any)'] = 'admin/school_class/edit/$1';
$route['about'] = 'admin/setting/about';
$route['enquiry-list'] = 'admin/setting/enquiry';
$route['site-info'] = 'admin/setting/site_setting';
$route['book-set-combo'] = 'home/book_set_combo';
//$route['loose-books/(:num)'] = 'home/loose_books';
$route['about-us'] = 'home/about';
$route['contact-us'] = 'home/contact';
//$route['login'] = 'home/login';
$route['order'] = 'admin/order';
$route['myorder'] = 'order';
$route['invoice/(:any)'] ='order/invoice/$1';
$route['order/view_invoice/(:any)'] = 'admin/order/view_invoice/$1';
$route['product-detail/(:any)'] = 'home/product_detail/$1';
$route['classes/(:any)'] = 'home/classes/$1';
$route['book_classes/(:any)'] = 'home/book_classes/$1';
$route['book_sets/(:any)'] = 'home/book_sets/$1';
$route['checkout'] = 'cart/checkout';
$route['make-payment'] = 'CcavRequestHandler/send';
$route['confirmation-order/(:any)/(:any)'] = 'cart/confirmation_order/$1/$2';


