
 <div class="page-wraper">
	<!-- Header End -->
	<div class="page-content bg-white">

	<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?=base_url('home');?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		
		<!-- Client Start-->
		

		<!--Recommend Section Start-->
		<section class="content-inner-1 bg-grey reccomend">
			<div class="container">
				<h3 class="text-default text-center">BOOKS SET FOR</h3>
				<!-- Swiper -->
				
				<div class="row justify-content-center">
				<?php foreach($schools as $key=>$school) {
						?>
				    <div class="col-md-4">					  
						<a href="<?=base_url('classes/'.base64_encode($school->id));?>">
							<div class="books-card style-1 wow fadeInUp" data-wow-delay="0.1s" >
								<div class="dz-media">
									<img src="<?= $school->image != "" ? base_url($school->image) : base_url('public/website/images/dummy_image.jpg')?> " style="width:100%;" alt="book">								
								</div>
								<div class="content">
									<h6 class="title"><?= $school->name; ?></h6>
								</div>
							</div>
						</a>
						
					</div>
					<?php } ?>
				</div>
				
			</div>
		</section>
		
		
		
	

		
	</div>