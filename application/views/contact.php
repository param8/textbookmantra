
	<div class="page-wraper">
		
		
		
		<div class="page-content">
			<!-- inner page banner -->
			<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?=base_url('home');?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
				
		
			<section class="contact-wraper1" style="background-image: url(images/background/bg2.jpg);">	
				<div class="container">
					<div class="row">
						<div class="col-lg-5">
							<div class="contact-info">
								<div class="section-head text-dark style-1">
									<h3 class="title text-dark">Get In Touch</h3>
									<p>If you are interested in working with us, please get in touch.</p>
								</div>
								<ul class="no-margin">
									<li class="icon-bx-wraper text-dark left m-b30">
										<div class="icon-md">
											<span class="icon-cell text-primary">
											<i class="flaticon-placeholder"></i>
												<!-- <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg> -->
											</span>
										</div>
										<div class="icon-content">
											<h5 class=" dz-tilte text-dark">Our Address</h5>
											<p style="text-transform: uppercase;"><?=$siteinfo->address?></p>
										</div>
									</li>
									<li class="icon-bx-wraper text-dark left m-b30">
										<div class="icon-md">
											
											<span class="icon-cell text-primary">
											<i class="flaticon-email"></i>	
											<!-- <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg> -->
											</span>
										</div>
										<div class="icon-content">
											<h5 class="dz-tilte text-dark">Our Email</h5>
											<p><?=$siteinfo->email?></p>
										</div>
									</li>
									<li class="icon-bx-wraper text-dark left m-b30">
										<div class="icon-md">
											<span class="icon-cell text-primary">
											<i class="flaticon-phone"></i>
												<!-- <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg> -->
											</span>
										</div>
										<div class="icon-content">
											<h5 class=" dz-tilte text-dark">Our Contact</h5>
											<a href="tel:<?=$siteinfo->mobile?>" class="text-dark"><p><?=$siteinfo->mobile?></p></a>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-7 m-b40">
							<div class="contact-area1 m-r20 m-md-r0">
								<div class="section-head style-1">
									<h6 class="sub-title text-primary">CONTACT US</h6>
									<h3 class="title m-b20">Get In Touch With Us</h3>
								</div>
								<form id="addContact" method="POST" action="<?=base_url('admin/setting/store_contact')?>">
										
									<div class="input-group">
										<input required type="text" class="form-control" name="name" id="name" placeholder="Full Name">
									</div>
									<div class="input-group">
										<input required type="text" class="form-control" name="email" id="email" placeholder="Email Adress">
									</div>
									<div class="input-group">
										<input required type="tel" class="form-control" name="mobile" id="mobile" placeholder="Phone No." maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
									</div>
									<div class="input-group">
										<textarea name="message" id="message" rows="5" class="form-control" placeholder="Message"></textarea>
									</div>
									<div>
										<input name="submit" type="submit" value="SUBMIT" class="btn w-100 btn-primary btnhover">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-- Feature Box -->
			<section class="content-inner-2">
				<div class="container">
					<div class="row sp15">
						<div class="col-lg-3 col-md-6 col-sm-6 col-6">
							<div class="icon-bx-wraper style-2 m-b30 text-center">
								<div class="icon-bx-lg">
									<i class="fa-solid fa-users icon-cell"></i>
								</div>
								<div class="icon-content">
									<h2 class="dz-title counter m-b0">125,663</h2>
									<p class="font-20">Happy Customers</p>
								</div>
							</div>
						</div>
						<div class=" col-lg-3 col-md-6 col-sm-6 col-6">
							<div class="icon-bx-wraper style-2 m-b30 text-center">
								<div class="icon-bx-lg"> 
									<i class="fa-solid fa-book icon-cell"></i>
								</div>
								<div class="icon-content">
									<h2 class="dz-title counter m-b0">50,672</h2>
									<p class="font-20">Book Collections</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 col-6">
							<div class="icon-bx-wraper style-2 m-b30 text-center">
								<div class="icon-bx-lg"> 
									<i class="fa-solid fa-store icon-cell"></i>
								</div>
								<div class="icon-content">
									<h2 class="dz-title counter m-b0">1,562</h2>
									<p class="font-20">Our Stores</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 col-6">
							<div class="icon-bx-wraper style-2 m-b30 text-center">
								<div class="icon-bx-lg"> 
									<i class="fa-solid fa-leaf icon-cell"></i>
								</div>
								<div class="icon-content">
									<h2 class="dz-title counter m-b0">457</h2>
									<p class="font-20">Famous Writers</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Feature Box End -->
			<div class="map-iframe">
				    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.1763889449844!2d77.28657751723753!3d28.654436741831745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfb5ba7c0eb0d%3A0x41cc6d529ce89a7b!2sPratap%20Publishers!5e0!3m2!1sen!2sin!4v1675760730817!5m2!1sen!2sin" style="border:0; width:100%; min-height:100%; margin-bottom: -8px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
					
				</div>
			<!-- Newsletter -->
			<section class="py-5 newsletter-wrapper" style="background-image: url('images/background/bg1.jpg'); background-size: cover;">
				<div class="container">
					<div class="subscride-inner">
						<div class="row style-1 justify-content-xl-between justify-content-lg-center align-items-center text-xl-start text-center">
							<div class="col-xl-7 col-lg-12">
								<div class="section-head mb-0">
									<h2 class="title text-dark my-lg-3 mt-0">Subscribe our newsletter for newest books updates</h2>
								</div>
							</div>
							<div class="col-xl-5 col-lg-6">
								<form class="dzSubscribe style-1" action="" method="post">
									<div class="dzSubscribeMsg"></div>
									<div class="form-group">
										<div class="input-group mb-0">
											<input name="dzEmail" required="required" type="email" class="form-control bg-transparent text-dark" placeholder="Your Email Address">
											<div class="input-group-addon">
												<button name="submit" value="Submit" type="submit" class="btn btn-primary btnhover">
													<span>SUBSCRIBE</span>
													<i class="fa-solid fa-paper-plane"></i>
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Newsletter End -->
				
		</div>
		
		
		
		<button class="scroltop" type="button"><i class="fas fa-arrow-up"></i></button>
	</div>
<script>
$("form#addContact").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.reload();
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});

</script>