<header class="site-header mo-left header style-1">
    <div class="top-header">
    <p class="text-center">partap publisher & distributor our online store</p>
  </div>
  <!-- Main Header -->
  <div class="header-info-bar" style="display:none">
    <div class="container clearfix">
      <!-- Website Logo -->
      <div class="logo-header logo-dark">
        <a href="<?= base_url('home');?>"><img src="<?=base_url($siteinfo->image)?>" alt="logo"></a>
      </div>
      <!-- EXTRA NAV -->
      <div class="extra-nav">
        <div class="extra-cell">
          <ul class="navbar-nav header-right">
            <?php $session_data = $this->session->userdata(); 
              if(empty($session_data['email'] )){?>
            <li class="nav-item">
              <div class="dropdown-footer" >
                <button type="button" class="btn btn-success w-100 btnhover btn-sm" onclick="loginModal()" >Log In</button>	
              </div>
            </li>
            <?php }else{?>
            <!-- <li class="nav-item" id="wish_div">
              <a class="nav-link" href="<?= base_url('wishlist'); ?>">
              	<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
              	<span class="badge"><?=$totalwishlist?></span>
              </a>
              </li> -->
            <li class="nav-item" id="items_div">
            <a href="<?= base_url('cart'); ?>">
              <button type="button" class="nav-link box " id="total_items">                
                  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000">
                    <path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                  </svg>                
                <span class="badge" ><?php echo $this->cart->total_items()?></span>
              </button>
              </a>
            </li>
            <li class="nav-item dropdown profile-dropdown  ms-4">
              <a class="nav-link" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <!--<img src="<?//=base_url('public/website/images/user1.jpg')?>" alt="/">-->
                <i class="fa fa-user"></i>
                <div class="profile-info">
                  <h6 class="title"><?=$session_data['name'];?></h6>
                  <span><?=$session_data['email'];?></span>
                </div>
              </a>
              <div class="dropdown-menu py-0 dropdown-menu-end">
                <div class="dropdown-header">
                  <h6 class="m-0"><?=$session_data['name'];?></h6>
                  <span><?=$session_data['email'];?></span>
                </div>
                <div class="dropdown-body">
                  <!-- <a href="my-profile.html" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                    	<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 6c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2m0 10c2.7 0 5.8 1.29 6 2H6c.23-.72 3.31-2 6-2m0-12C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 10c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>
                    	<span class="ms-2">Profile</span>
                    </div>
                    </a> -->
                  <a href="<?= base_url('myorder'); ?>" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000">
                        <path d="M0 0h24v24H0V0z" fill="none"/>
                        <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                      </svg>
                      <span class="ms-2">My Order</span>
                    </div>
                  </a>
                  <!-- <a href="<?=base_url('wishlist');?>" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                    	<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
                    	<span class="ms-2">Wishlist</span>
                    </div>
                    </a> -->
                </div>
                <div class="dropdown-footer">
                  <a class="btn btn-primary w-100 btnhover btn-sm" href="<?=base_url('Authantication/logout')?>">Log Out</a>
                </div>
              </div>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Modal Start -->
  <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Login</h5>
          <button onclick="closeModal()" class="close btn btn-default" >
          <span aria-hidden="true"><i class="fa fa-times"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?=base_url('Authantication/login')?>" id="Frontlogin" method="post" >
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Email:</label>
              <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Password:</label>
              <input type="password" class="form-control" id="password" name="password"><i class="far fa-eye" id="togglePassword" style="cursor: pointer;position: absolute;right: 25px;bottom: 28px;"></i>
            </div>
        </div>
        <div class="row mb-2">
        <div class="col-md-12">
        <div class="container">
        <button type="submit"  class="btn btn-primary">Log In</button> 
        </div>
        </div>
        <div class="col-md-12 ">
        <div class="container">
        <p> Do not have any acoount?<a href="javascript:void(0);" onclick="registerModal()" > Register Now</a>
        </div>
        </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!-- Login Modal End -->
  <!-- Register Modal Start -->
  <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Register</h5>
          <button onclick="closeModal()" class="close btn btn-default" >
          <span aria-hidden="true"><i class="fa fa-times"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?=base_url('Authantication/register')?>" id="Frontregister" method="post" >
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Name:</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Email:</label>
              <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Password:</label>
              <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Mobile:</label>
              <input type="text" class="form-control" id="mobile" maxlength="10" minlength="10" name="mobile" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Address:</label>
              <input type="text" class="form-control" id="address" name="address">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Pin Code:</label>
              <input type="text" class="form-control" id="pincode" name="pincode" maxlength="6" minlength="6" oninput="this.value = this.value.replace(/[^0-5]/g, '').replace(/(\..*)\./g, '$1');">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">State:</label>
              <select  class="form-control" id="state" name="state" onchange="getCity(this.value)">
                <option value="">Choose State...</option>
                <?php foreach($states as $state){?>
                <option value="<?=$state->id?>"><?=$state->name?></option>
                <?php  } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">City:</label>
              <select class="form-control" id="city" name="city">
                <option value="">Choose City...</option>
              </select>
            </div>
        </div>
        <div class="row mb-2">
        <div class="col-md-12">
        <div class="container">
        <button type="submit" class="btn btn-info" >Register</button>
        </div>
        </div>
        <div class="col-md-12 ">
        <div class="container">
        <p> Already have an acoount?<a href="javascript:void(0);" onclick="loginNow()" > Login Now</a>
        </div>
        </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!-- Register Modal End -->
  <!-- Main Header End -->
  <!-- Main Header -->
  <div class="sticky-header main-bar-wraper navbar-expand-lg">
    <div class="main-bar clearfix">
      <div class="container clearfix">
        <!-- Website Logo -->
        <div class="logo-header logo-dark">
          <a href="<?=base_url('home');?>"><img src="<?=base_url($siteinfo->image)?>" alt="logo"></a>
        </div>
        <!-- Nav Toggle Button -->
        <button class="navbar-toggler collapsed navicon justify-content-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
        </button>
        <!-- EXTRA NAV -->
        <!--<div class="extra-nav">-->
        <!--  <div class="extra-cell">-->
        <!--    <a href="#" class="btn btn-warning btnhover"  onclick="loginModal()">Login</a>	-->
        <!--  </div>-->
        <!--</div>-->
        
              <div class="extra-nav">
        <div class="extra-cell">
          <ul class="navbar-nav header-right">
            <?php $session_data = $this->session->userdata(); 
              if(empty($session_data['email'] )){?>
            <li class="nav-item">
              <div class="dropdown-footer d-flex" >
                <button type="button" class="btn btn-success w-50 btnhover btn-sm" onclick="loginModal()" >Log In</button>	&nbsp;
                <button type="button" class="btn btn-info w-50 btnhover btn-sm" onclick="registerModal()" >Register</button>	
              </div>
            </li>
            <?php }else{?>
            <!-- <li class="nav-item" id="wish_div">
              <a class="nav-link" href="<?= base_url('wishlist'); ?>">
              	<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
              	<span class="badge"><?=$totalwishlist?></span>
              </a>
              </li> -->
            <li class="nav-item" id="items_div">
            <a href="<?= base_url('cart'); ?>">
              <button type="button" class="nav-link box " id="total_items">                
                  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000">
                    <path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                  </svg>                
                <span class="badge" ><?php echo $this->cart->total_items()?></span>
              </button>
              </a>
            </li>
            <li class="nav-item dropdown profile-dropdown  ms-4">
              <a class="nav-link" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <!--<img src="<?//=base_url('public/website/images/user1.jpg')?>" alt="/">-->
                <i class="fa fa-user-circle fa-xl"></i>
                <div class="profile-info">
                  <h6 class="title"><?=$session_data['name'];?></h6>
                  <span><?=$session_data['email'];?></span>
                </div>
              </a>
              <div class="dropdown-menu py-0 dropdown-menu-end">
                <div class="dropdown-header">
                  <h6 class="m-0"><?=$session_data['name'];?></h6>
                  <span><?=$session_data['email'];?></span>
                </div>
                <div class="dropdown-body">
                  <!-- <a href="my-profile.html" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                    	<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 6c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2m0 10c2.7 0 5.8 1.29 6 2H6c.23-.72 3.31-2 6-2m0-12C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 10c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>
                    	<span class="ms-2">Profile</span>
                    </div>
                    </a> -->
                  <a href="<?= base_url('myorder'); ?>" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000">
                        <path d="M0 0h24v24H0V0z" fill="none"/>
                        <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                      </svg>
                      <span class="ms-2">My Order</span>
                    </div>
                  </a>
                  <!-- <a href="<?=base_url('wishlist');?>" class="dropdown-item d-flex justify-content-between align-items-center ai-icon">
                    <div>
                    	<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
                    	<span class="ms-2">Wishlist</span>
                    </div>
                    </a> -->
                </div>
                <div class="dropdown-footer">
                  <a class="btn btn-primary w-100 btnhover btn-sm" href="<?=base_url('Authantication/logout')?>">Log Out</a>
                </div>
              </div>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
        <!-- Main Nav -->
        <div class="header-nav navbar-collapse collapse justify-content-start" id="navbarNavDropdown">
          <div class="logo-header logo-dark">
            <a href="<?=base_url('home');?>"><img src="<?=base_url($siteinfo->image)?>" alt=""></a>
          </div>
          <form class="search-input">
            <div class="input-group">
              <input type="text" class="form-control" aria-label="Text input with dropdown button" placeholder="Search Books Here">
              <button class="btn" type="button"><i class="flaticon-loupe"></i></button>
            </div>
          </form>
          
          <ul class="nav navbar-nav">
            <li ><a href="<?=base_url('home');?>"><span>Home</span></a></li>
            <li><a href="<?=base_url('book-set-combo');?>"><span>Book Set Combo</span></a></li>
            <li><a href="javascript:void(0)<?//=base_url('home/loose_books_schools');?>"><span>Loose Books</span></a></li>
            <li style="display:none"><a href="<?=base_url('home/stationary');?>"><span>Stationary</span></a></li>
            <li><a href="<?=base_url('about-us');?>"><span>Information</span></a></li>
            <li><a href="<?=base_url('contact-us');?>"><span>Contact Us</span></a></li>
          </ul>
          <div class="dz-social-icon">
            <ul>
              <li><a class="fab fa-facebook-f" target="_blank" href="<?= $siteinfo->facebook_url?>"></a></li>
              <li><a class="fab fa-twitter" target="_blank" href="<?= $siteinfo->twitter_url?>"></a></li>
              <li><a class="fab fa-linkedin-in" target="_blank" href="<?= $siteinfo->linkdin_url?>"></a></li>
              <li><a class="fab fa-youtube" target="_blank" href="<?= $siteinfo->youtube_url?>"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Main Header End -->
</header>
<script>
   const togglePassword = document.querySelector('#togglePassword');
   const password = document.querySelector('#password');

  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});
  </script>