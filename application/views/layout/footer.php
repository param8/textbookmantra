<footer class="site-footer style-1">
		
<!-- Footer Top -->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-lg-12 wow fadeInUp" data-wow-delay="0.1s">
						<div class="widget widget_about">
							<div class="footer-logo logo-white">
								<a href="<?=base_url('home');?>"><img src="<?=base_url($siteinfo->image)?>" alt="" style="height:100px"></a> 
							</div>
							<p class="text"><?= $siteinfo->site_about;?> </p>
							<div class="dz-social-icon style-1">
								<ul>
									<li><a href="<?= $siteinfo->facebook_url?>" target="_blank" ><i class="fa-brands fa-facebook-f"></i></a></li>
									<li><a href="<?= $siteinfo->youtube_url?>" target="_blank"><i class="fa-brands fa-youtube"></i></a></li>
									<li><a href="<?= $siteinfo->linkdin_url?>" target="_blank"><i class="fa-brands fa-linkedin"></i></a></li>
									<li><a href="<?= $siteinfo->twitter_url?>" target="_blank"><i class="fa-brands fa-twitter"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-6 col-md-4 col-sm-6 col-6 wow fadeInUp" data-wow-delay="0.2s">
						<div class="widget widget_services">
							<h5 class="footer-title">Our Links</h5>
							<ul>
								<li><a href="<?=base_url('about-us');?>">About us</a></li>
								<li><a href="<?=base_url('contact-us');?>">Contact us</a></li>
								<!-- <li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Pricing Table</a></li>
								<li><a href="#">FAQ</a></li> -->
							</ul>
						</div>
					</div>
					<div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-6 wow fadeInUp" data-wow-delay="0.4s">
						<div class="widget widget_services">
							<h5 class="footer-title">Resources</h5>
							<ul>
								<!-- <li><a href="#">Download</a></li>
								<li><a href="#">Help Center</a></li> -->
								<li><a href="#" onclick="redirectCart()" >Shop Cart</a></li>
								<!-- <li><a href="#">Partner</a></li> -->
								<li><a href="#">Book Set Combo</a></li>
								<li><a href="<?=base_url('home/loose_books_schools');?>">Loose Books</a></li>
								<li><a href="#">Partner</a></li>
							</ul>
						</div>
					</div>
					<div class="col-xl-4 col-lg-3 col-md-12 col-sm-10 wow fadeInUp" data-wow-delay="0.5s">
						<div class="widget widget_getintuch">
							<h5 class="footer-title">Get in Touch With Us</h5>
							<ul>
								<li>
									<i class="flaticon-placeholder"></i>
									<span style="text-transform: uppercase;"><?=$siteinfo->address?></span>
								</li>
								<li>
									<i class="flaticon-phone"></i>
									<a href="tel:<?=$siteinfo->mobile?>" class="text-default"><span><?=$siteinfo->mobile?></span></a>
								</li>
								<li>
									<i class="fab fa-whatsapp"></i>
									<?=$siteinfo->whatsapp_no?></span>
								</li>
								<li>
									<i class="flaticon-email"></i> 
									<span><?=$siteinfo->email?></span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row fb-inner">
					<div class="col-lg-12 col-md-12 text-center"> 
						<p class="copyright-text"> <?=$siteinfo->footer_data?></p>
					</div>
					<!-- <div class="col-lg-6 col-md-12 text-end"> 
						<p>Made with by <a href="#">Webcadence</a></p>
					</div> -->
				</div>
			</div>
		</div>
		<!-- Footer Bottom End -->
		
	</footer>
	<!-- Footer End -->
	
	<button class="scroltop" type="button"><i class="fas fa-arrow-up"></i></button>
</div>
<!-- JAVASCRIPT FILES ========================================= -->
<script src="<?=base_url('public/website/js/jquery.min.js')?>"></script><!-- JQUERY MIN JS -->
<script src="<?=base_url('public/website/vendor/wow/wow.min.js')?>"></script><!-- WOW JS -->
<script src="<?=base_url('public/website/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')?>"></script><!-- BOOTSTRAP MIN JS -->
<script src="<?=base_url('public/website/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')?>"></script><!-- BOOTSTRAP SELECT MIN JS -->
<script src="<?=base_url('public/website/vendor/counter/waypoints-min.js')?>"></script><!-- WAYPOINTS JS -->
<script src="<?=base_url('public/website/vendor/counter/counterup.min.js')?>"></script><!-- COUNTERUP JS -->
<script src="<?=base_url('public/website/vendor/swiper/swiper-bundle.min.js')?>"></script><!-- SWIPER JS -->
<script src="<?=base_url('public/website/js/dz.carousel.js')?>"></script><!-- DZ CAROUSEL JS -->
<script src="<?=base_url('public/website/js/dz.ajax.js')?>"></script><!-- AJAX -->
<script src="<?=base_url('public/website/js/custom.js')?>"></script><!-- CUSTOM JS -->

<!-- Bootstrap JS -->
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>
<script>
	function loginModal(){
		$('#modalLogin').modal('show');
		$('div').removeClass('modal-backdrop');
	}
</script>
	<script>
		function closeModal(){
		  $('#modalLogin').modal('hide');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		function loginNow(){
		  $('#modalLogin').modal('show');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		
		function registerModal(){
		  $('#modalRegister').modal('show');
		  $('#modalLogin').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		
	</script>
	<script>
		$("form#Frontlogin").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?php current_url() ?>";
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
//     $("form#Frontlogin").submit(function(e) {
// 		alert('ddddd');
     
//    });
    </script>

<script>
		$("form#Frontregister").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?=base_url('home')?>";
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
     
		function getCity(stateID){
		$.ajax({
		url: '<?=base_url('Ajax_controller/get_city')?>',
		type: 'POST',
		data: {stateID},
		success: function (data) {
			$('#city').html(data);
            $('.city').html(data);
		}
     });
  }
    </script>

	<script>
	

function addTocart(productID,losseBook){
		var sessions = '<?= $this->session->userdata('email');?>';
		if(sessions != ''){
			var book_type = losseBook;
			$.ajax({
			url: '<?=base_url('cart/store_cart')?>',
			type: 'POST',
			data:{productID,book_type},
			dataType: 'json',
			success: function (data) {
			if(data.status==200){
			toastr.success("Item Added successfully");
			location.reload();		
			}
			},
			error: function(){} 
			});
    }else{
		$('#modalLogin').modal('show');
		$('div').removeClass('modal-backdrop');
	}
}

//    function addTowishlist(productID){
// 	var sessions = '<?= $this->session->userdata('email');?>';
//      $.ajax({
//        url: '<?=base_url('Wishlist/wishstore_cart')?>',
//        type: 'POST',
// 	   data:{productID},
// 	   dataType: 'json',
//        success: function (data) {
// 	   if(data.status==200){
// 		toastr.success(data.message);
//     	$('#wish_div').load(location.href + ' #wish_div');		
// 	   }
//        },
//        error: function(){} 
//      });
//    }

   function update_cart(qty,rowid){
	
	$.ajax({
       url: '<?=base_url('cart/update_cart')?>',
       type: 'POST',
	   data:{qty,rowid},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
		setTimeout(function(){
		location.reload();
			}, 1000) 
    	//$('#items_div').load(location.href + ' #items_div');		
	   }
       },
       error: function(){} 
     });
   }
   function removeCartItem(rowid){
	
	$.ajax({
       url: '<?=base_url('cart/removeCartItem')?>',
       type: 'POST',
	   data:{rowid},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
			toastr.success('Item Removed..');
    	$('#items_div').load(location.href + ' #items_div');
		location.reload();		
	   }
       },
       error: function(){} 
     });
   }

   function removeWishItem(id){
	$.ajax({
       url: '<?=base_url('Wishlist/removeWishItem')?>',
       type: 'POST',
	   data:{id},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
			toastr.success(data.message);
			setTimeout(function(){
             location.reload();
           }, 1000) 
  		
	   }else{
		toastr.error(data.message);
	   }
       },
       error: function(){} 
     });
   }

   function redirectCart(){
	var session = '<?= $this->session->userdata('email')?>';
	if(session == ""){
		$('#modalLogin').modal('show');
		$('div').removeClass('modal-backdrop');
	}else{
		window.location='cart';
	}
   }

	</script>
</body>
</html>