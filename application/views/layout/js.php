<!-- JAVASCRIPT FILES ========================================= -->
<script src="<?=base_url('public/website/js/jquery.min.js')?>"></script><!-- JQUERY MIN JS -->
<script src="<?=base_url('public/website/vendor/wow/wow.min.js')?>"></script><!-- WOW JS -->
<script src="<?=base_url('public/website/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')?>"></script><!-- BOOTSTRAP MIN JS -->
<script src="<?=base_url('public/website/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')?>"></script><!-- BOOTSTRAP SELECT MIN JS -->
<script src="<?=base_url('public/website/vendor/counter/waypoints-min.js')?>"></script><!-- WAYPOINTS JS -->
<script src="<?=base_url('public/website/vendor/counter/counterup.min.js')?>"></script><!-- COUNTERUP JS -->
<script src="<?=base_url('public/website/vendor/swiper/swiper-bundle.min.js')?>"></script><!-- SWIPER JS -->
<script src="<?=base_url('public/website/js/dz.carousel.js')?>"></script><!-- DZ CAROUSEL JS -->
<script src="<?=base_url('public/website/js/dz.ajax.js')?>"></script><!-- AJAX -->
<script src="<?=base_url('public/website/js/custom.js')?>"></script><!-- CUSTOM JS -->

<!-- Bootstrap JS -->
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'></script>

	<script>
		function registerModal(){
		  $('#modalRegister').modal('show');
		  $('#modalLogin').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		
	</script>
	<script>
		$("form#Frontlogin").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?=base_url('home')?>";
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
//     $("form#Frontlogin").submit(function(e) {
// 		alert('ddddd');
     
//    });
    </script>

<script>
		$("form#Frontregister").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?=base_url('home')?>";
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
     
		function getCity(stateID){
		$.ajax({
		url: '<?=base_url('Ajax_controller/get_city')?>',
		type: 'POST',
		data: {stateID},
		success: function (data) {
			$('#city').html(data);
		}
     });
  }
    </script>

	<script>
	function addTocart(productID){
	var sessions = '<?= $this->session->userdata('email');?>';
    if(sessions==""){
		$('#modalRegister').modal('hide');
		  $('#modalLogin').modal('show');
		  $('div').removeClass('modal-backdrop');
	}else{
     $.ajax({
       url: '<?=base_url('cart/store_cart')?>',
       type: 'POST',
	   data:{productID},
	   dataType: 'json',
       success: function (data) {
	   if(data.status==200){
    	$('#items_div').load(location.href + ' #items_div');		
	   }
       },
       error: function(){} 
     });
   }
}

   function addTowishlist(productID){
	var sessions = '<?= $this->session->userdata('email');?>';
    if(sessions==""){
		$('#modalRegister').modal('hide');
		  $('#modalLogin').modal('show');
		  $('div').removeClass('modal-backdrop');
	}else{
     $.ajax({
       url: '<?=base_url('Wishlist/wishstore_cart')?>',
       type: 'POST',
	   data:{productID},
	   dataType: 'json',
       success: function (data) {
	   if(data.status==200){
    	$('#wish_div').load(location.href + ' #wish_div');		
	   }
       },
       error: function(){} 
     });
   }
   }

   function update_cart(qty,rowid){
	
	$.ajax({
       url: '<?=base_url('cart/update_cart')?>',
       type: 'POST',
	   data:{qty,rowid},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
    	$('#items_div').load(location.href + ' #items_div');		
	   }
       },
       error: function(){} 
     });
   }
   function removeCartItem(rowid){
	
	$.ajax({
       url: '<?=base_url('cart/removeCartItem')?>',
       type: 'POST',
	   data:{rowid},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
    	$('#items_div').load(location.href + ' #items_div');
		location.reload();		
	   }
       },
       error: function(){} 
     });
   }

   function removeWishItem(id){
	alert(id);
	// $.ajax({
    //    url: '<?=base_url('Wishlist/removeWishItem')?>',
    //    type: 'POST',
	//    data:{id},
	//    dataType: 'json',
    //    success: function (data) {
	// 	if(data.status==200){
    // 	//$('#wish_div').load(location.href + ' #wish_div');
	// 	//location.reload();		
	//    }
    //    },
    //    error: function(){} 
    //  });
   }

	</script>