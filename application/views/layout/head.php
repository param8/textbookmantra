<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="DexignZone" />
	<meta name="robots" content="" />
	<meta name="description" content="Textbookmantra.com"/>
	<meta property="og:title" content="Textbookmantra.com"/>
	<meta property="og:description" content="Textbookmantra.com"/>


	<!-- FAVICONS ICON -->
	<link rel="icon" type="image/x-icon" href="<?=base_url($siteinfo->image)?>"/>
	
	<!-- PAGE TITLE HERE -->
	<title><?=$page_title?></title>
	
	<!-- MOBILE SPECIFIC -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- STYLESHEETS -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/website/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/website/icons/fontawesome/css/all.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/website/vendor/swiper/swiper-bundle.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/website/vendor/animate/animate.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/website/css/style.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('icons/themify/themify-icons.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('vendor/bootstrap-select/dist/css/bootstrap-select.min.css')?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	
	<!-- GOOGLE FONTS-->
	<link rel="preconnect" href="https://fonts.googleapis.com/">
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&amp;family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">

	
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<style>
/*.nav-item.dropdown a{*/
/*    background-color: #adcd0f52!important;*/
/*    padding: 5px!important;*/
/*}*/
	.transform-uppercase{
		text-transform: uppercase;
	}
	p.copyright-text {
    color: #fff;
}
.overflow:hover {
  overflow: visible;
}

.overflow:hover span {
  position: relative;
  background-color: white;
  font-size: 15px;

  box-shadow: 0 0 4px 0 black;
  border-radius: 1px;
}
.swiper-slide img {
    height: 400px;
	width:100%;
}
.newsletter-wrapper{
	display:none;
}
.top-header{
	background: #dcdcdcb0;
    text-transform: uppercase;
    font-weight: 600;
    font-size: 22px;
    font-family: math;
    letter-spacing: 1px;
	}
.top-header p{
	padding-top: 5px;
    color: #222466;
    margin-bottom:0px!important;
	}
</style>
</head>	
<body>