<div class="page-wraper">
	<div id="loading-area" class="preloader-wrapper-1">
		<div class="preloader-inner">
			<div class="preloader-shade"></div>
			<div class="preloader-wrap"></div>
			<div class="preloader-wrap wrap2"></div>
			<div class="preloader-wrap wrap3"></div>
			<div class="preloader-wrap wrap4"></div>
			<div class="preloader-wrap wrap5"></div>
		</div> 
	</div>
	<div class="error-page overlay-secondary-dark" style="background-image: url(images/background/bg3.jpg);">
	
		<div class="error-inner text-center">
		    
			<h3><?=$order_status=='Success' ? '<span class="text-success">Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.</span>' : 
				($order_status=='Failure' ? '<span class="text-danger">Thank you for shopping with us.However,the transaction has been declined</span>' : '<span class="text-warning">Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail</span>') ?></h3>
			<h2 class="error-head">Your Order Id is <div class="dz_error" data-text="<?=$order_id?>">#<?=$order_id?></div></h2>
			<a href="<?=base_url();?>" class="btn btn-primary btn-border btnhover white-border">BACK TO HOMEPAGE</a>
		</div>
	</div>
</div>