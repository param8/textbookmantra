
<div class="page-content">
		<!-- inner page banner -->
			<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?= base_url(); ?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- inner page banner End-->
            <!-- contact area -->
			<section class="invoice printableArea">
		  <div class="row">
			<div class="col-12 no-print">
			  <div class="bb-1 clearFix">
					<div class="text-end pb-15">
						<!-- <button class="btn btn-success" type="button"> <span><i class="fa fa-print"></i> Save</span> </button> -->
						<button id="print2" class="btn btn-warning" type="button" onclick="printInvoice();"> <span><i class="fa fa-print"></i> Print</span> </button>
					</div>	
			  	</div>
			</div>
			<div id="print_invoice" class="container">
			<div class="col-12">
			<div class="page-header">
				<h2 class="d-inline"><span class="font-size-30 text-dark">Invoice Detail</span></h2>
				<div class="pull-right text-right text-dark">
					<strong>Date:</strong>	<?php echo date('d/m/Y',strtotime($order_details->created_at));?>
				</div>	
			  </div>
			<!-- /.col -->
		  </div>
		  <div class="row">
				<div class="col-md-6 cols-6">
					<strong>From</strong>	
					<div>
						<?php// print_r($order_details);die;?>
						<strong class="text-blue font-size-24"><?=$siteinfo->name;?></strong><br>
						<?=$siteinfo->address?><br>
						<p><strong>Phone: <?=$siteinfo->mobile;?> &nbsp;&nbsp;&nbsp;&nbsp;<br> Email: <?=$siteinfo->email;?></strong> </p> 
					</div>
				</div>
			<!-- /.col -->
			<div class="col-md-6 cols-6">
			  <strong>Shipping Address</strong>
			  <div>
					<?php //print_r($shipping_details);die;
					$shipping_detail = json_decode($shipping_details->payment_detail); //json_decode($order_details->payment_detail);
					//echo "<pre>";
					//print_r($shipping_detail);die;
					$data1 = array();
					foreach($shipping_detail as $row){
						
						foreach($row as $key1=>$data){
							$data1[$key1]=$data;
						}
						//print_r($data1['delivery_name']);
					}
					//print_r($data1['delivery_name']);
				
					?>
					<strong class="text-blue font-size-24 text-capitalize"><?=$data1['delivery_name']?></strong><br>
					<?=$data1['delivery_address'];?><br>
					<?=$data1['delivery_city'];?>, <?=$data1['delivery_state'];?>, <?=$data1['delivery_zip'];?><br>
					<strong>Phone: <?=$data1['delivery_tel'];?> &nbsp;&nbsp;&nbsp;&nbsp; <br>Email: <?=$data1['billing_email'];?></strong> -
					<?php //}?>  
				</div>
			</div>
			<!-- /.col -->
			<div class="col-sm-12 invoice-col mb-15 pl-160">
				<div class="invoice-details row no-margin">
				  <div class="col-md-6 col-lg-6"><b>Order ID:</b> <?=$data1['order_id'];?></div>
				  <div class="col-md-6 col-lg-6 text-right"><b>Payment Status:</b> <?=$data1['order_status'];?></div>
				</div>
			</div>
		  <!-- /.col -->
		  </div>
		  <div class="row">
			<div class="col-12 table-responsive">
			  <table class="table table-bordered">
				<tbody>
				<tr>
				  <th>#</th>
				  <th>School</th>
				  <th>Class</th>
				  <th>Book Set</th>
				  <th>Details</th>
				  <th class="text-right">Quantity</th>
				  <th class="text-right">Unit Cost</th>
				  <th class="text-right">Subtotal</th>
				</tr>
				<?php
					$i=1;
					$total=0;
					$subtotal=0;
					foreach($order_items as $item){
						$total = $item->qty*$item->mrp;
						$subtotal += $total;
				?>
				<tr>
				  <td><?=$i++;?></td>
				  <td><?=$item->school_name;?></td>
				  <td><?=$item->school_class;?></td>
				  <td><?=$item->title;?></td>
				  <td>
					<?php 
						$item_ids = explode(',',$item->items);
						$i=1;
						foreach($item_ids as $item_id){
							echo $i. ". " . getItemNameByItemId($item_id)."<br>";
							$i++;
						}
				  	?>
				  </td>
				  <td class="text-right"><?=$item->qty;?></td>
				  <td class="text-right">₹<?=$item->mrp;?></td>
				  <td class="text-right">₹<?=number_format((float)$total, 2, '.', '');?></td>
				</tr>
				<tr>
				<?php
						$tax=0;
						$shipping_charge=0;
						$tax = $order_details->gst;
						$shipping_charge = $order_details->shipping_charge;					
					?>
					<td colspan="3"><strong>Tax (GST)  :  ₹<?=number_format((float)$tax, 2, '.', '');?></strong></td>
					<td colspan="2"><strong>Shipping Charge :  ₹<?=number_format((float)$shipping_charge, 2, '.', '');?></strong></td>
					<td colspan="3"><strong>Sub - Total amount  :  ₹<?=number_format((float)$subtotal, 2, '.', '');?></strong></td>
				</tr>
				<tr>
					<td colspan="8" class="text-center"><strong >Total Amount : ₹<?= number_format((float)$order_details->total_amount , 2, '.', '');?></strong></td>
				</tr>
				<?php } ?>
				</tbody>
			  </table>
			</div>
			<!-- /.col -->
		  </div>
		  </div>
		  <div class="row no-print">
			<div class="col-12 text-center">
			  <button type="button" class="btn btn-success" onclick="window.history.go(-1); return false;"><i class="fa fa-backward"></i> Back
			  </button>
			</div>
		  </div>
		</section>
		<!-- contact area End--> 
	
</div>

<script>
	function printInvoice(){
		var getFullContent = document.body.innerHTML;
                var printsection = document.getElementById('print_invoice').innerHTML;
                document.body.innerHTML = printsection;
                window.print();
                document.body.innerHTML = getFullContent;
	}
</script>