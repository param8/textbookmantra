<div class="page-content bg-grey">
		<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?=base_url('home');?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		<div class="content-inner-1 border-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="d-flex  align-items-center mb-3">
							<div class="col-xl-6"><h4 class="title text-left"><?=$page_title?></h4></div>
							<form class="d-flex justify-content-end align-items-center col-xl-6" action="<?=base_url("home/LooseBooks/".base64_encode($class->id))?>" method="POST" id="search_book">
								<div class="col-xl-6">
									<input type="text" class="form-control" value="<?= $this->session->userdata('search') ?>" name="search"  placeholder="Search Any Book..." style="border-radius: 6px 0 0 5px;">
									
								</div>
								<div class="col-xl-2">
									<input class="btn btn-primary " type="submit" name="submit" style="border-radius: 0 5px 5px 0;" value="Search">
								</div>
								<input class="text-white btn btn-dark" type="reset" style="cursor:pointer" onclick="resetSearchFilter()" value="Reset Filter">
								
							</form>
						</div>
						
						
						<div class="row book-grid-row">
                            <?php 
                            //print_r($loose_books);die();
							if(count($loose_books) > 0){
                            foreach($loose_books as $key => $book){
                            
                            ?>
							<div class="col-book style-1">
								<div class="dz-shop-card style-1">
									<div class="dz-media">
									 <a href="<?= base_url('product-detail/'.base64_encode($book->id))?>">
										<img src="<?=base_url($book->image) ?>" alt="book">	
							         </a>									
									</div>
									<div class="dz-content">
										<h5 class="text-primary"><?=$book->title?></h5>
									
										<div class="book-footer">
										    <h5 class="text-primary"><?=$book->title?></h5>
											<div class="price">
												<span class="price-num"><i class="fa fa-inr"></i><?=$book->special_price?></span>
												<!-- <del>$54.0</del> -->
											</div>
											<a href="javascript:void(0);" onclick="addTocart('<?=$book->id; ?>','Loose Book')" class="btn btn-warning box-btn btnhover btnhover2"><i class="flaticon-shopping-cart-1 m-r10"></i> Add to cart</a>
										</div>
									</div>
								</div>
                                
							</div>
						<?php }
						if(empty($this->session->userdata('search'))){?>
                        <div class="row page mt-0">
					<div class="col-md-6">
						<p class="page-text" style="display:none">Showing <?//=$per_page?> from <?//=$total_rows?> data</p>
					</div>
					<div class="col-md-6">
					<nav aria-label="Blog Pagination">
                        <?php// print_r($links)?>
						</nav>
					</div>
				</div>
            <?php }}else{?>
							<div><img src="<?=base_url('public/website/images/NoRecordFound.jpg')?>"></div>
							<?php } ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		
		
	</div>
	
	<script>
		$("form#search_book").submit(function(e) {
		
		var formData = new FormData(this);
		//console.log(formData);die();
		$.ajax({
		  url: '<?=base_url("home/searchBook")?>',
		  type: 'POST',
		  data: formData,
		  cache: false,
		  contentType: false,
		  processData: false,
		  dataType: 'json',
		   success: function (data) {
			
			//location.reload();
		  },
	   });
	  });

	  function resetSearchFilter(){
      $.ajax({
       url: '<?=base_url("home/resetSearchFilter")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        window.location="<?=base_url('home/LooseBooks/'.base64_encode($class->id))?>";
       },
     });
    }
	</script>