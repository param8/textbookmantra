<div class="page-content bg-grey">
<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?=base_url('home');?>"> Home</a></li>
								<li class="breadcrumb-item"><a href="<?=base_url('home/loose_books_schools');?>"> Loose Book Schools</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
								      
							</ul>
						</nav>
					</div>
				</div>
			</div>
		<div class="content-inner-1 border-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="d-flex justify-content-between align-items-center">
							<h4 class="title"><span class="text-primary">CLASSES OF </span>[<?=$school->name?>]</h4>
							<a href="javascript:void(0);" class="btn btn-primary panel-btn">Filter</a>
						</div>
							
						<div class="row book-grid-row">
                            <?php 
                            if(count($classes) > 0){
                            foreach($classes as $key => $class){
                            ?>
							<div class="col-book style-1">
								<div class="dz-shop-card style-1">
								<a href="<?= base_url('home/LooseBooks/'.base64_encode($class->id))?>">
									<div class="dz-media">
										<img src=<?=base_url($class->image) ?> alt="book" style="height:140px">							
									</div>
									<div class="bookmark-btn style-2">
										<input class="form-check-input" type="checkbox" id="flexCheckDefault1">
										
									</div>
									<div class="dz-content">
										<h5 class="title"><a href="<?= base_url('home/LooseBooks/'.base64_encode($class->id))?>"><?=$class->class?></a></h5>
									</div>
							    </a>
								</div>
							</div>
						<?php } }else{?>
							<div><img src="<?=base_url('public/website/images/NoRecordFound.jpg')?>"></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	