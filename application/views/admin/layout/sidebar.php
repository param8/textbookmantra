<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
	  	<div class="multinav">
		  <div class="multinav-scroll" style="height: 100%; overflow:auto">	
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">	
				<li class="header">Dashboard</li>
				<li class="">
				  <a href="<?=base_url('dashboard')?>">
					<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
					<span>Dashboard</span>
				</li>
		
				<li class="header">Components </li>
				<li >
				  <a href="<?=base_url('users')?>">
					<i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
					<span>Users</span>
				</li>
				<li class="treeview">
				  <a href="#">
					<i class="fa fa-university"><span class="path1"></span><span class="path2"></span></i>
					<span>School Detail</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					<li><a href="<?=base_url('school')?>"><i class="icon-Speaker"><span class="path1"></span><span class="path2"></span></i>Schools</a></li>
					<li><a href="<?=base_url('class')?>"><i class="icon-Speaker"><span class="path1"></span><span class="path2"></span></i>Classes</a></li>  
					<li><a href="<?=base_url('item')?>"><i class="icon-Speaker"><span class="path1"></span><span class="path2"></span></i>Items</a></li>  
					<li><a href="<?=base_url('book-set')?>"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Book Set</a></li>  
				</ul>
				</li>
				
				<li>
				  <a href="<?=base_url('order')?>">
					<i class="fa fa-shopping-cart"><span class="path1"></span><span class="path2"></span></i>
					<span>Orders</span>
				</li>

				<li>
				  <a href="<?=base_url('enquiry-list')?>">
					<i class="fa fa-info-circle"><span class="path1"></span><span class="path2"></span></i>
					<span>Enquiry List</span>
				</li>

				<li class="treeview">
				  <a href="#">
					<i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
					<span>Settings</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-right pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu">
					<li><a href="<?=base_url('language')?>"><i class="icon-Speaker"><span class="path1"></span><span class="path2"></span></i>Language</a></li>
					<li><a href="<?=base_url('slider')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>Slider</a></li>
					<li><a href="<?=base_url('about')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>About US</a></li>
					<li><a href="<?=base_url('site-info')?>"><i class="icon-Image"><span class="path1"></span><span class="path2"></span></i>Site Setting</a></li>  
				</ul>
				</li>  		
			  </ul>
		  </div>
		</div>
    </section>
  </aside>