
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
           
          <div class="box "> 
            <div class="box-header with-border">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                  <h3 class="box-title">All <?=$page_title?></h3>
                </div>
                <div class="col-md-6 col-lg-6 ">
                  <!-- <a href="#" class="btn btn-success btn-sm float-right ml-2" data-toggle="modal" data-target="#importSchools" data-whatever="@mdo">Import Schools <i class="fa fa-plus"></i></a> -->
                  <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addSchoolModal" data-whatever="@mdo">Add School <i class="fa fa-plus"></i></a>
                </div>
              </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				</div>
        <div class="box"> 
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>Image</th>
								<th>Name</th>
								<th>District</th>
								<th>State</th>
								<th>Status</th>
								<th>Created Date</th>
                				<th>Action</th>
							</tr>
						</thead>
						<tbody>
            				<?php foreach($schools as $key=>$school){?>
							<tr>
								<td><?=$key+1;?></td>
                <td style="width: 20%;"><img src="<?=$school->image;?>" alt="book" style="width: 85%; margin-bottom: 10px;"></td>
								<td><?= $school->name?></td>
								<td><?= $school->cities_name?></td>
								<td><?= $school->state_name?></td>
								<td><?= $school->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">De-Active</span>'?></td>
                <td><?= date('d-m-Y',strtotime($school->create_date));?></td>
                <td>
                  <a href="#" onclick="editModalShow('<?=$school->school_u_id?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit School"><i class="fa fa-edit"></i></a>
                  <a href="#" onclick="deleteSchool('<?=$school->school_u_id?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete School"><i class="fa fa-trash"></i></a>
                </td>
							</tr>
							<?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Schoo Modal Start -->
  <div class="modal fade" id="addSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add School</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/school/store')?>" id="addSchool" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <label for="state" class="col-form-label">Address:</label>
            <textarea  class="form-control" name="address" id="address"> </textarea>
          </div>
          <div class="form-group">
            <label for="state" class="col-form-label">State:</label>
            <select class="form-control" name="state" id="state" onchange="getCity(this.value )">
              <option value="">Select State</option>
              <?php foreach($states as $state){?>
                <option value="<?=$state->id?>"><?=$state->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="city" class="col-form-label">District:</label>
            <select class="form-control" name="city" id="city">
              <option value="">Select District</option>
            </select>
          </div>
        
          <div class="form-group">
            <label for="image" class="col-form-label">School Picture:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Modal End -->

   <!---------import file modal start----------->
   <div class="modal fade" id="importSchools" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Import Schools</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/school/importSchools')?>" id="importItemsForm" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="form-group">
          <label for="school" class="col-form-label">School:</label>
            <select class="form-control" name="school" id="school" onchange="getClasses(this.value)">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>"><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
          <label for="class" class="col-form-label">Class:</label>
            <select class="form-control class" name="class" id="class" >
              <option value="">Select Class</option>
            </select>
          </div>
        
          <div class="form-group">
            <label for="importFile" class="col-form-label">Upload File:</label>
            <input type="file" class="form-control" name="importFile" id="importFile">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!---------import file end----------->


  <!-- Edit Schoo Modal Start -->
  <div class="modal fade" id="editSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit School</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/school/update')?>" id="editSchool" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editFormData">
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Modal End -->

  
  <script type="text/javascript">
  function getCity(stateID){
    $.ajax({
       url: '<?=base_url('Ajax_controller/get_city')?>',
       type: 'POST',
       data: {stateID},
       success: function (data) {
         $('#city').html(data);
         $('.city').html(data);
       }
     });
  }

  function editModalShow(uid){
    $.ajax({
       url: '<?=base_url('admin/school/editForm')?>',
       type: 'POST',
       data: {uid},
       success: function (data) {
        $('#editSchoolModal').modal('show');
         $('#editFormData').html(data);
       }
     });
  }

  $("form#addSchool").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add school');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   $("form#importSchoolForm").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add school');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   $("form#editSchool").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit school');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   function deleteSchool(uid){
     var messageText  = "You want delete this school!";
     var confirmText =  'Yes, delete it!';
     var message  ="School delete Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/school/delete')?>', 
                method: 'POST',
                data: {uid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }
  
</script>