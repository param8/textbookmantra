
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		 <!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
					<div class="box bg-primary-light">
						<div class="box-body d-flex px-0">
							<div class="flex-grow-1 p-20 flex-grow-1 bg-img dask-bg bg-none-md" style="background-position: right bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-1.svg)">
								<div class="row">
									<div class="col-12 col-xl-7">
										<h2>Welcome back, <strong><?php echo $this->session->name?></strong></h2><br>
									</div>
									<div class="col-12 col-xl-5"></div>
									</div>
									<div class="col-12 row">
										<div class="col-xl-3">
											<div class="box">
												<div class="box-body d-flex p-0">
													<div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
														<a href="<?=base_url('admin/school')?>"><h4 class="font-weight-500 text-white text-center"><i class="fa fa-university"></i> <?= $schools; ?></h4>
														<h4 class="font-weight-400 text-white text-center">Total Schools</h4></a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xl-3">
											<div class="box">
												<div class="box-body d-flex p-0">
													<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
														<a href="<?=base_url('class')?>"><h4 class="font-weight-500 text-white text-center"><i class="fa fa-users"></i> <?=$classes?></h4>
														<h4 class="font-weight-400 text-white text-center">Total Classes</h4>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xl-3">
											<div class="box">
												<div class="box-body d-flex p-0">
													<div class="flex-grow-1 bg-warning p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
													<a href="<?=base_url('item')?>"><h4 class="font-weight-500 text-white text-center"><i class="fa fa-book"></i> <?=$books?></h4>
														<h4 class="font-weight-400 text-white text-center">Total Items</h4></a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xl-3">
											<div class="box">
												<div class="box-body d-flex p-0">
													<div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
													<a href="<?=base_url('admin/book-set')?>"><h4 class="font-weight-500 text-white text-center"><i class="fa fa-book"></i> <?php echo $booksets;?></h4>
														<h4 class="font-weight-400 text-white text-center">Total Booksets</h4></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								
							</div>
						</div>
					</div>
														
				</div>
				
				
			</div>
		 </section>
		 <!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->


  

	
	

