<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Invoice</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Invoice</li>
								<li class="breadcrumb-item active" aria-current="page">Invoice Sample</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>  

		<!-- Main content -->
		<section class="content">
			<div class="invoice printableArea">
		  <div class="row">
			<div class="col-12 no-print">
			  <div class="bb-1 clearFix">
				<div class="text-right pb-15">
					<!-- <button class="btn btn-success" type="button"> <span><i class="fa fa-print"></i> Save</span> </button> -->
					<button id="print2" class="btn btn-warning" type="button" onclick="printInvoice();"> <span><i class="fa fa-print"></i> Print</span> </button>
				</div>	
			  </div>
			</div>
			<div class="col-12">
			  <div class="page-header">
				<h2 class="d-inline"><span class="font-size-30">Invoice Details</span></h2>
				<div class="pull-right text-right">
					<h3><?=date('d F Y', strtotime($order_details->create_date));?></h3>
				</div>	
			  </div>
			</div>
			<!-- /.col -->
		  </div>
		  <div class="row invoice-info">
			<div class="col-md-6 invoice-col">
			  <strong>From</strong>	
			  <address>
				<strong class="text-blue font-size-24"><?=$siteinfo->name;?></strong><br>
				<?=$siteinfo->address;?><br>
				<strong>Phone: <?=$siteinfo->mobile;?> &nbsp;&nbsp;&nbsp;&nbsp; Email: <?=$siteinfo->email;?></strong>  
			  </address>
			</div>
			<!-- /.col -->
			<div class="col-md-6 invoice-col text-right">
			  <strong>Billing To</strong>
				<?php $shipping_detail =json_decode($shipping_details->payment_detail);
				//print_r($shipping_detail);die;
				foreach($shipping_detail as $row){
					
					foreach($row as $key1=>$data){
						$data1[$key1]=$data;
					}
					//print_r($data1['delivery_name']);
				}

				?>
			  <address>
				<strong class="text-blue font-size-24"><?=$data1['delivery_name']?></strong><br>
				<?=$data1['delivery_address'];?><br>
				<?=$data1['delivery_city'];?>, <?=$data1['delivery_state'];?>, <?=$data1['delivery_zip'];?><br>
				<strong>Phone: <?=$data1['delivery_tel'];?> &nbsp;&nbsp;&nbsp;&nbsp; Email: <?=$data1['billing_email'];?></strong>
			  </address>
			</div>
			<!-- /.col -->
			<div class="col-sm-12 invoice-col mb-15">
				<div class="invoice-details row no-margin">
				  <div class="col-md-6 col-lg-6"><b>Order ID:</b> <?=$data1['order_id'];?></div>
				  <div class="col-md-6 col-lg-6 text-right"><b>Payment Status:</b> <?=$data1['order_status'];?></div>
				</div>
			</div>
		  <!-- /.col -->
		  </div>
		  <div class="row">
			<div class="col-12 table-responsive">
			  <table class="table table-bordered">
				<tbody>
				<tr>
				  <th>#</th>
				  <th>School</th>
				  <th>Class</th>
				  <th>Book Set</th>
				  <th>Details</th>
				  <th class="text-right">Quantity</th>
				  <th class="text-right">Unit Cost</th>
				  <th class="text-right">Subtotal</th>
				</tr>
				<?php
					$i=1;
					$total=0;
					$subtotal=0;
					foreach($order_items as $item){
						$total = $item->qty*$item->mrp;
						$subtotal += $total;
				?>
				<tr>
				  <td><?=$i++;?></td>
				  <td><?=$item->school_name;?></td>
				  <td><?=$item->school_class;?></td>
				  <td><?=$item->title;?></td>
				  <td>
					<?php 
						$item_ids = explode(',',$item->items);
						$i=1;
						foreach($item_ids as $item_id){
							echo $i. ". " . getItemNameByItemId($item_id)."<br>";
							$i++;
						}
				  	?>
				  </td>
				  <td class="text-right"><?=$item->qty;?></td>
				  <td class="text-right">₹<?=$item->mrp;?></td>
				  <td class="text-right">₹<?=number_format((float)$total, 2, '.', '');?></td>
				</tr>
				<?php } ?>
				</tbody>
			  </table>
			</div>
			<!-- /.col -->
		  </div>
		  <div class="row">
			<div class="col-12 text-right">
				<div>
					<?php
						$tax=0;
						$shipping_charge=0;
						$tax = $subtotal*0/100;
						$shipping_charge = $order_details->shipping_charge;					
					?>
					<p>Sub - Total amount  :  ₹<?=number_format((float)$subtotal, 2, '.', '');?></p>
					<p>Tax (18%)  :  ₹<?=number_format((float)$tax, 2, '.', '');?></p>
					<p>Shipping  :  ₹<?=number_format((float)$shipping_charge, 2, '.', '');?></p>
				</div>
				<div class="total-payment">
					<h3><b>Total :</b> ₹<?=number_format((float)($subtotal+$tax+$shipping_charge), 2, '.', '');?></h3>
				</div>

			</div>
			<!-- /.col -->
		  </div>
		  <div class="row no-print">
			<div class="col-12 text-center">
			  <button type="button" class="btn btn-success" onclick="window.history.go(-1); return false;"><i class="fa fa-backward"></i> Back
			  </button>
			</div>
		  </div>
		  </div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
<script>
	function printInvoice() {
		window.print();
	}
</script>