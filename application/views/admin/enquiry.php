
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
                <div class="box"> 
                    <div class="box-header">
                        <h3 class="box-title">All <?=$page_title?></h3>
                    </div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
						<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Message</th>
									<th>Created Date</th>
								</tr>
							</thead>
							<tbody>
						<?php foreach($enquiries as $key=>$enquiry){?>
								<tr>
									<td><?=$key+1;?></td>
									<td><?= $enquiry->name?></td>
									<td><?= $enquiry->email?></td>
									<td><?= $enquiry->mobile?></td>
									<td><?= $enquiry->message?></td>
									<td><?= date('d-m-Y',strtotime($enquiry->created_at));?></td>
								</tr>
						<?php } ?>
					
							</tbody>				  
						
						</table>
						</div>              
					</div>
					<!-- /.box-body -->
			   </div>
			  <!-- /.box -->  
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->