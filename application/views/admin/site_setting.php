<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
        </div>
        <!-- /.box-header -->
        <div class="">
          <form action="<?=base_url('admin/setting/store_siteInfo')?>" id="addSiteInfo" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label for="s_id" class="col-form-label">Site Name:</label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$siteinfo->name?>"> 
              </div>
              <div class="form-group">
                <label for="language_id" class="col-form-label">Mobile No.:</label>
                <input type="text" class="form-control" maxlength="10" minlength="10" name="mobile" id="mobile" value="<?=$siteinfo->mobile?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"> 
              </div>
              <div class="form-group">
                <label for="classname" class="col-form-label">Whatsapp No:</label>
                <input type="text" class="form-control" maxlength="10" minlength="10" name="whatsapp_no" id="whatsapp_no" value="<?=$siteinfo->whatsapp_no?>" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Email:</label>
                <input type="email" class="form-control" name="email" id="email" value="<?=$siteinfo->email?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Address:</label>
                <textarea  class="form-control" name="address" id="address"><?=$siteinfo->address?></textarea>
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Site About:</label>
                <textarea  class="form-control" name="site_about" id="site_about"><?=$siteinfo->site_about?></textarea>
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Facebook URL:</label>
                <input type="text" class="form-control" name="facebook_url" id="facebook_url" value="<?=$siteinfo->facebook_url?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Youtube URL:</label>
                <input type="text" class="form-control" name="youtube_url" id="youtube_url" value="<?=$siteinfo->youtube_url?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Linkedin URL:</label>
                <input type="text" class="form-control" name="linkdin_url" id="linkdin_url" value="<?=$siteinfo->linkdin_url?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Twitter URL:</label>
                <input type="text" class="form-control" name="twitter_url" id="twitter_url" value="<?=$siteinfo->twitter_url?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Instagram URL:</label>
                <input type="text" class="form-control" name="insta_url" id="insta_url" value="<?=$siteinfo->insta_url?>">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Footer Data:</label>
                <input type="text" class="form-control" name="footer_data" id="footer_data" value="<?=$siteinfo->footer_data?>">
              </div>
              <div class="form-group">
                <label for="image" class="col-form-label">Logo Image:</label>
                <input type="file" class="form-control" name="image" id="image">
              </div>
              <div>
                <image src="<?=$siteinfo->image?>" width="100" height="100">
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->          
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $("form#addSiteInfo").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
</script>