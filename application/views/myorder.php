<?php //print_r($this->session->userdata());?>
<div class="page-content">
		<!-- inner page banner -->
			<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1>Order's</h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?= base_url(); ?>"> Home</a></li>
								<li class="breadcrumb-item">My Order</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- inner page banner End-->
            <!-- contact area -->
    <section class="content-inner shop-account">
			<!-- Product -->
			<div class="container">
				<div class="row mb-5">
					<div class="col-lg-12">
						<div class="table-responsive">
							<table class="table check-tbl">
								<thead>
									<tr>
										<th>Order Id</th>
										<th>Quantity</th>
										<th>Amount</th>
										<th >Status</th>
										<th>Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                <?php
									if(count($orders) > 0) {
										//print_r($orders);
									foreach($orders as $order){
										$status = $order->payment_status==0 ? '<span class="tetx-danger">Pending</span>' : ($order->payment_status==1 ? '<span class="tetx-warning">In-Progress</span>' : '<span class="tetx-success">Delivered</span>' ); 
										?>
									<tr>
										<td class="product-item-no"><?= $order->order_id;?></td>
										<td class="product-item-name"><?= $order->set_qty;?></td>
										<td class="product-item-price"><?= $order->total_amount?></td>
										<td><?=$status?></td>
										<td><?=date('d-m-Y',strtotime($order->created_at))?></td>
										<td><a href="<?=base_url('invoice/'.base64_encode($order->id))?>" ><i class="fa fa-eye"></i></a></td>
									</tr>
                                    <?php }}else{
										echo "<tr>
										<td colspan='6' class='text-center'>No Orders Found ..</td></tr>";
									} ?>
									
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- <div class="row">
					<div class="col-lg-6">
						<div class="widget">
							<form class="shop-form"> 
								<h4 class="widget-title">Calculate Shipping</h4>
								<div class="form-group">
									<select class="default-select">
										  <option selected>Credit Card Type</option>
										  <option value="1">Another option</option>
										  <option value="2">A option</option>
										  <option value="3">Potato</option>
									</select>	
								</div>	
								<div class="row">
									<div class="form-group col-lg-6">
										<input type="text" class="form-control" placeholder="Credit Card Number">
									</div>
									<div class="form-group col-lg-6">
										<input type="text" class="form-control" placeholder="Card Verification Number">
									</div>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Coupon Code">
								</div>
								<div class="form-group">
									<a href="shop-cart.html" class="btn btn-primary btnhover" type="button">Apply Coupon</a>
								</div>
							</form>	
						</div>
					</div>
					<div class="col-lg-6">
						<div class="widget">
							<h4 class="widget-title">Cart Subtotal</h4>
							<table class="table-bordered check-tbl m-b25">
								<tbody>
									<tr>
										<td>Order Subtotal</td>
										<td><?=$this->cart->total()?></td>
									</tr>
									<tr>
										<td>Shipping</td>
										<td>Free Shipping</td>
									</tr>
									<tr>
										<td>Coupon</td>
										<td>$28.00</td>
									</tr>
									<tr>
										<td>Total</td>
										<td>$506.00</td>
									</tr>
								</tbody>
							</table>
							<div class="form-group m-b25">
								<a href="shop-checkout.html" class="btn btn-primary btnhover" type="button">Proceed to Checkout</a>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			<!-- Product END -->
	</section>
		<!-- contact area End--> 
	
</div>