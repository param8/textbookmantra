<div class="page-content">
  <!-- inner page banner -->
  <div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
    <div class="container">
      <div class="dz-bnr-inr-entry">
        <h1><?=$page_title?></h1>
        <nav aria-label="breadcrumb" class="breadcrumb-row">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url(); ?>"> Home</a></li>
            <li class="breadcrumb-item"><?=$page_title?></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <!-- inner page banner End-->
  <!-- contact area -->
  <section class="content-inner shop-account">
    <!-- Product -->
    <div class="container">
      <div class="row mb-5">
        <div class="col-lg-12">
          <div class="table-responsive">
            <table class="table check-tbl">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Total</th>
                  <th >Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $cartItems=$this->cart->contents();
                  	if(count($cartItems) > 0) {
                                             //print_r($cartItems);
                  foreach($cartItems as $item){?>
                <tr>
                  <td class="product-item-img"><img src="<?= $item['image']?>" alt=""></td>
                  <td class="product-item-name"><?= $item['items'] == 0 ? $item['name'] : $item['name']?></td>
                  <td class="product-item-price"><?= $item['price']?></td>
                  <td class="product-item-quantity">
                    <div class="quantity btn-quantity style-1 me-3">
                      <input id="" type="text" style="width: 50px;" value="<?= $item['qty']?>" onchange="update_cart(this.value,'<?=$item['rowid']?>')" minlenght="1" maxlength="3" name="demo_vertical2" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" min="1"/>
                    </div>
                  </td>
                  <td class="product-item-totle"><i class="fa fa-inr"><?= $item['subtotal']?></i></td>
                  <td class="float-right">
                  <?php if($item['items'] == 0 ){?>
                    <a href="javascript:void(0);" onclick="removeCartItem('<?=$item['rowid']?>')"class="text-danger"><i class="fa fa-times"></i></a>
                    <?php }else{?>
                    <a href="javascript:void(0);" onclick="cartDetail('<?=$item['rowid']?>')"class=""><i class="fa fa-eye"></i></a>
                    <a href="javascript:void(0);" onclick="removeCartItem('<?=$item['rowid']?>')"class="text-danger"><i class="fa fa-times"></i></a>
                  <?php }?>
                  </td>
                </tr>
                <?php }}else{
										echo "<tr>
										<td colspan='6' class='text-center'>Cart is Empty ..</td></tr>";
									}  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-lg-6">
          <div class="widget">
            <h4 class="widget-title">Cart Subtotal</h4>
            <table class="table-bordered check-tbl m-b25">
              <tbody>
                <tr>
                  <td>Order Subtotal</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> <?=$this->cart->total()?></i></td>
                </tr>
                <tr>
                  <td>Shipping Charge</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> 200</i></td>
                </tr>
                <tr>
                  <td>Coupon</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> 0</i></td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td><i class="fa fa-inr" aria-hidden="true"> <?=($this->cart->total()+200)?></i></td>
                </tr>
              </tbody>
            </table>
            <div class="form-group m-b25">
              <!--<a href="<?=base_url('checkout')?>" class="btn btn-primary btnhover" type="button">Proceed to Checkout</a>-->
              <a href="<?=base_url('checkout')?>" class="btn btn-primary btnhover" type="button">Proceed to Checkout</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Product END -->
  </section>
  <!-- contact area End--> 
</div>

<div class="modal fade" id="cart_detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="margin-top:10px!important">
      <div class="modal-content" >
        <div id="show_cart_detail"></div>

      <div class="modal-footer">
        <button type="button" onclick="CloseModal()" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </div>
      
  </div>
</div>

<script>
   function cartDetail(rowid){
    $.ajax({
        url: '<?=base_url('cart/cart_detail')?>',
        type: 'POST',
        data:{rowid},
        success: function (data) {
            $('#cart_detailModal').modal('show');
            $('#show_cart_detail').html(data);
        },
      });
   }
   function CloseModal(){
    $('#show_cart_detail').html('');
    $('#cart_detailModal').modal('hide');
   }
</script>