
 <div class="page-wraper">
	<div id="loading-area" class="preloader-wrapper-1">
		<div class="preloader-inner">
			<div class="preloader-shade"></div>
			<div class="preloader-wrap"></div>
			<div class="preloader-wrap wrap2"></div>
			<div class="preloader-wrap wrap3"></div>
			<div class="preloader-wrap wrap4"></div>
			<div class="preloader-wrap wrap5"></div>
		</div> 
	</div>
	
	<!-- Header -->
	
	<!-- Header End -->
	<div class="page-content bg-white">

	   <!--Swiper Banner Start -->
		<div class="main-slider style-1"> 
			<div class="main-swiper">
				<div class="swiper-wrapper">
					<?php 
					foreach($sliders as $slider){ //print_r($slider);
						?>
						 <!-- <img src="<?=base_url(!empty($slider->image)?'public/uploads/'.$slider->image:'public/website/images/banner/banner-media.png')?>"> -->
					<div class="swiper-slide" >
					<a href="<?=base_url($slider->btn_link)?>"><img src="<?=base_url(!empty($slider->image)?'public/uploads/'.$slider->image:'public/website/images/banner/banner-media.png')?>" alt="banner-media"></a>
								
					</div>
					<?php }?>
				</div>
				<div class="container swiper-pagination-wrapper">
					<div class="swiper-pagination-five"></div>
				</div>
			</div>
			<div class="swiper main-swiper-thumb">
				<!--<div class="swiper-wrapper">-->
					
				<!--	<div class="swiper-slide">-->
				<!--	</div>-->
				<!--</div>-->
			</div>
		</div>		
		<!--Swiper Banner End-->
		
		<!-- Client Start-->
		

		<!--Recommend Section Start-->
		<section class="content-inner-1 bg-grey reccomend">
			<div class="container">
				<h3 class="text-default text-center">BOOKS SET FOR</h3>
				<!-- Swiper -->
				
				<div class="row justify-content-center">
				<?php foreach($schools as $key=>$school) {
						?>
				    <div class="col-md-3">					  
						<a href="<?=base_url('classes/'.base64_encode($school->id));?>">
							<div class="books-card style-1 wow fadeInUp" data-wow-delay="0.1s" >
								<div class="dz-media">
									<img src="<?= $school->image != "" ? base_url($school->image) : base_url('public/website/images/dummy_image.jpg')?> " style="width:100%;" alt="book">								
								</div>
								<div class="content">
									<h6 class="title"><?= $school->name; ?></h6>
								</div>
							</div>
						</a>
						
					</div>
					<?php } ?>
				</div>
				
			</div>
		</section>
		
		<!-- icon-box1 -->
		<section class="content-inner-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
						<div class="icon-bx-wraper style-1 m-b20 text-center">
							<div class="icon-bx-sm m-b10">
								<i class="flaticon-power icon-cell"></i>
							</div>
							<div class="icon-content">
								<h5 class="dz-title m-b10">Quick Delivery</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.2s">
						<div class="icon-bx-wraper style-1 m-b20 text-center">
							<div class="icon-bx-sm m-b10"> 
								<i class="flaticon-shield icon-cell"></i>
							</div>
							<div class="icon-content">
								<h5 class="dz-title m-b10">Secure Payment</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
						<div class="icon-bx-wraper style-1 m-b20 text-center">
							<div class="icon-bx-sm m-b10"> 
								<i class="flaticon-like icon-cell"></i>
							</div>
							<div class="icon-content">
								<h5 class="dz-title m-b10">Best Quality</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
						<div class="icon-bx-wraper style-1 m-b20 text-center">
							<div class="icon-bx-sm m-b10"> 
								<i class="flaticon-star icon-cell"></i>
							</div>
							<div class="icon-content">
								<h5 class="dz-title m-b10">Return Guarantee</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- icon-box1 End-->
		
	
		<!-- Book Sale End -->
		
		<!-- Feature Product -->
		
		<!-- Feature Product End -->
		
	

		<!-- Newsletter -->
		<!-- <section class="py-5 newsletter-wrapper" style="background-image: url('<?=base_url("public/website/images/background/bg1.jpg")?>'); background-size: cover;">
			<div class="container">
				<div class="subscride-inner">
					<div class="row style-1 justify-content-xl-between justify-content-lg-center align-items-center text-xl-start text-center">
						<div class="col-xl-7 col-lg-12 wow fadeInUp" data-wow-delay="0.1s">
							<div class="section-head mb-0">
								<h2 class="title text-white my-lg-3 mt-0">Subscribe our newsletter for newest books updates</h2>
							</div>
						</div>
						<div class="col-xl-5 col-lg-6 wow fadeInUp" data-wow-delay="0.2s">
							<form class="dzSubscribe style-1" action="https://bookland.dexignzone.com/xhtml/script/mailchamp.php" method="post">
								<div class="dzSubscribeMsg"></div>
								<div class="form-group">
									<div class="input-group mb-0">
										<input name="dzEmail" required="required" type="email" class="form-control bg-transparent text-white" placeholder="Your Email Address">
										<div class="input-group-addon">
											<button name="submit" value="Submit" type="submit" class="btn btn-primary btnhover">
												<span>SUBSCRIBE</span>
												<i class="fa-solid fa-paper-plane"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- Newsletter End -->
		
	</div>
	<script>
	function loginModal(){
		$('#modalLogin').modal('show');
		$('div').removeClass('modal-backdrop');
	}
	// $( document ).ready(function() {
	// 	$(window).on('load',function(){
	// if(!sessionStorage.getItem('show')){
	// 	loginModal();
	// 	sessionStorage.setItem('show', 'true');
	// }
	// });
			
// });
	</script>
	<!-- Footer -->
	
		<!-- Footer Top End -->
		
		<!-- Footer Bottom -->