
<div class="page-content">
		<!-- inner page banner -->
		<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
			<div class="container">
				<div class="dz-bnr-inr-entry">
					<h1><?=$page_title?></h1>
					<nav aria-label="breadcrumb" class="breadcrumb-row">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?= base_url(); ?>"> Home</a></li>
							<li class="breadcrumb-item"><?=$page_title?></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- inner page banner End-->
		<div class="content-inner-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="table-responsive">
							<table class="table check-tbl">
									<thead>
										<tr>
											<th>Image</th>
											<th>Product name</th>
											<th>Price</th>
											<!-- <th>Quantity</th> -->
											<th>Action</th>
										</tr>
									</thead>
								<tbody>
								<?php
								 //print_r($wishlist);
								foreach($WishItems as $key=>$wishList) {
                                       ?>
									<tr>
										<td class="product-item-img"><img src="<?= $wishList->image != "" ? base_url($wishList->image) : base_url('public/website/images/dummy_image.jpg')?>" alt=""></td>
										<td class="product-item-name"><?= $wishList->title; ?></td>
										<td class="product-item-price"><?= $wishList->price; ?></td>
										<!-- <td class="product-item-quantity">
											<div class="quantity btn-quantity style-1 me-3">
												<input id="demo_vertical2" type="text" value="1" name="demo_vertical2"/>
											</div>
										</td> -->
										<td class=""> <a href="" onclick="addTocart('<?= $wishList->bookSetID; ?>')" class=""><i class="fa fa-shopping-basket m-r10"></i></a><a href="#"  onclick="removeWishItem(<?= $wishList->id; ?>)" class="text-danger "><i class="fa fa-trash"></i></a></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	