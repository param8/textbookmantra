<div class="page-wraper">
  <div class="page-content bg-grey">
    <section class="content-inner-1">
      <div class="container">
        <div class="row book-grid-row style-4 m-b60">
          <div class="col">
            <form id="addTocart" action="<?=base_url('cart/store_productDetail_cart')?>" method="post">
            <div class="dz-box">
              <div class="dz-media">
                <img src="<?=base_url($bookSet->image);?>" alt="book" style="width: 500px;">
              </div>
              <div class="dz-content">
                <div class="dz-header">
                  <h3 class="title"><?=$bookSet->title?></h3>
                </div>
                <div class="dz-body">
                  <table class="table-responsive">
                    <tbody>
                    <?php foreach($items as $item){?>
                      <tr>
                        <td style="width: 10%; display:none"><input type="checkbox" class="checkboxAmount" name="item[]" id="item"  onclick="get_total_amount()" value="<?=$item['id']?>" checked></td>
                        <td style="width: 20%;display:none"><img src="<?=base_url($item['image']);?>" alt="book" style="width: 85%; margin-bottom: 10px;"></td>
                        <td style="width: 50%;"><?=$item['title']?></td>
                        
                        <td style="width: 20%;">
                          <div class="row cold-md-12"><i class="fa fa-inr col-md-4" style="padding: 17px 0 0 15px;width: 20%; "> </i><input type="text" id="itemAmount<?=$item['id']?>" value="<?=$item['special_price']?>" class="form-control col-md-8" readonly style="background:none;border:none;width:50%;padding: 0;">
                          </div>  
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                  
                  <input type="hidden" name="bookSetID" id="bookSetID" value="<?= $bookSet->id; ?>">
                  <div class="book-footer">
                    <div class="product-num">
                      <?php
                      // $this->session->userdata('email');
                      if(empty($this->session->userdata('email'))){?>
											<a href="#" onclick="addTocart('<?= $bookSet->id; ?>')" class="btn btn-warning btnhover btnhover2 btn-block m-r10">Add to cart</a>
                        <?php } else{?>
                      <input type="submit"   class="btn btn-warning btnhover btnhover2 btn-block m-r10" value="Add to cart">
                      <?php } ?>
                      <!-- <a href="javascript:void(0);" onclick="addTowishlist('<?//= $bookSet->id; ?>')" class="btn btn-danger btnhover btnhover2"><i class="fa fa-heart "></i> </a> -->
                    </div>
                    <div class="price float-right">
                      <h5 style="text-align: end;padding-right: 50px;"><span class="fa fa-inr"> </span><input type="text" class="totalAmount text-center" name="totalAmount"  class="form-control" readonly style="border:none;width:20%;background:none"></h5>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<script>
  $( document ).ready(function() {
  get_total_amount();
  });
    function get_total_amount(){
    var totalamount = 0;
   $('.checkboxAmount').each(function(){
     if($(this).prop('checked')){
       var id =$(this).val();
       var amount =  $('#itemAmount'+id).val();
       totalamount += +amount;  
     }  
   });
   if(totalamount > 0){
    $('.totalAmount').val(totalamount);
   }else{
    $('.totalAmount').val('0');
   }
  
  }

  $("form#addTocart").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?=base_url('cart')?>";
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			})
		});
  
  function productDetailAddTocart(productID){
   $.ajax({
     url: '<?=base_url('cart/store_cart')?>',
     type: 'POST',
  data:{productID},
  dataType: 'json',
     success: function (data) {
  if(data.status==200){
  	$('#items_div').load(location.href + ' #items_div');		
  }
     },
     error: function(){} 
   });
  
  }
</script>