<div class="page-content">
			<!-- inner page banner -->
			<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?=base_url();?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- inner page banner End-->
			
			<!-- contact area -->
			<section class="content-inner-1">
				<!-- Product -->
				<div class="container">
					<form class="shop-form" id="addAddress" action="<?=base_url('cart/store_shippingAddress')?>" method="POST">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="widget">
									<h4 class="widget-title">Billing & Shipping Address</h4>
							    
									<div class="row">
										<div class="form-group col-md-12">
											<input type="text" class="form-control" name="name" id="name" placeholder="Name">
										</div>
									</div>
                                    <div class="row">
										<div class="form-group col-md-6">
											<input type="email" class="form-control" name="email" id="email" placeholder="Email">
										</div>
										<div class="form-group col-md-6">
											<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone" maxlength="10" minlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
										</div>
									</div>
									<div class="form-group">
										<textarea  class="form-control" name="address" id="address" placeholder="Address"></textarea>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" name="street" id="street"  placeholder="Apartment, suite, unit etc.">
										</div>
                                        <div class="form-group col-md-6">
										    	<input type="text" class="form-control" name="pin_code" id="pin_code"  placeholder="Enter Pincode">
									    	
                                            <!--<select class="form-control" id="pin_code" name="pin_code">-->
                                            <!--    <option value="">Select Pincode</option>-->
                                            <!--    <?php// foreach($pincodes as $pincode){ ?>-->
                                            <!--        <option value="<?//=$pincode->id?>"><?//=$pincode->pin_code?></option>-->
                                            <!--    <?php// } ?>-->
                                            <!--</select>-->
										</div>
										
									</div>
									<div class="row">
										<div class="form-group col-md-6">
                                        <select class="form-control" id="state" name="state" onchange="getCity(this.value)">
                                                <option value="">Select State</option>
                                                <?php foreach($states as $state){ ?>
                                                    <option value="<?=$state->id?>"><?=$state->name?></option>
                                                <?php } ?>
                                            </select>
										</div>
										<div class="form-group col-md-6">
                                        <select class="form-control city" id="city" name="city">
                                                <option value="">Select City</option>
                                            </select>
										</div>
									</div>
									
									<button class="btn btn-primary btnhover mb-3" type="submit" >Add Address <i class="fa fa-arrow-circle-o-down"></i></button>
									<!-- <div id="create-an-account" class="collapse">
										 <p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
										<div class="form-group">
											<input type="password" class="form-control" placeholder="Password">
										</div> 
									</div> -->
                            
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<button class="btn btn-primary btnhover mb-3" type="button" data-bs-toggle="collapse" data-bs-target="#different-address">Ship to a different address <i class="fa fa-arrow-circle-o-down"></i></button>
								<div id="different-address" class="collapse">
									<div class="row">
                                        <?php 
										 $totalAddress = count($addresses);
										foreach($addresses as $address){?>
                                        <div class="col-lg-12 col-md-12">
                                          <input type="radio" class="" name="addressID" id="addressID" value="<?=$address->id?>" checked="checked">&nbsp;&nbsp;<span class="ml-2"><?= $address->name.', '.$address->mobile.', '.$address->email.', '.$address->address.', '.$address->street.', '.$address->pinCode.', '.$address->cityName.', '.$address->stateName ?><span style="cursor:pointer" onclick="deleteAddress(<?=$address->id?>)" class="text-danger"><i class="fa fa-trash" style="margin-left: 25px;"></i></span></span>
                                        </div>
                                        <hr>
                                        <?php } ?>
                                    </div>
							</div>
						</div>
					</form>
					<div class="dz-divider bg-gray-dark text-gray-dark icon-center  my-5"><i class="fa fa-circle bg-white text-gray-dark"></i></div>
					<div class="row">
						<div class="col-lg-6">
							<div class="widget">
								<h4 class="widget-title">Your Order</h4>
								<table class="table-bordered check-tbl">
									<thead class="text-center">
										<tr>
											<th>IMAGE</th>
											<th>PRODUCT NAME</th>
                                            <th>Quantity</th>
											<th>TOTAL</th>
										</tr>
									</thead>
									<tbody>
                                    <?php
                                        $cartItems=$this->cart->contents();                           
                                        foreach($cartItems as $item){
                                    ?>
										<tr>
											<td class="product-item-img"><img src="<?=base_url($item['image'])?>" alt=""></td>
											<td class="product-item-name"><?=$item['name']?></td>
                                            <td class="product-item-name"><?=$item['qty']?></td>
											<td class="product-price"><i class="fa fa-inr"></i><?=$item['price']?></td>
										</tr>
                                        <?php } ?>
								
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-6">
							<form class="shop-form widget">
								<h4 class="widget-title">Order Total</h4>
								<table class="table-bordered check-tbl mb-4">
									<tbody>
										<tr>
											<td>Order Subtotal</td>
											<td class="product-price"><i class="fa fa-inr" aria-hidden="true"> <?=$this->cart->total()?></i></td>
										</tr>
										<tr>
										 <td>Shipping Charge</td>
                                         <td><i class="fa fa-inr" aria-hidden="true"> <?php 
											echo $shipping_charge = 200;?></i></td>
										</tr>
										<tr>
											<td>Coupon</td>
											<td class="product-price"><i class="fa fa-inr" aria-hidden="true"> 0</i></td>
										</tr>
										<tr>
											<td>Total</td>
											<td class="product-price-total"><i class="fa fa-inr" aria-hidden="true"> <?php 
											echo $amount = ($this->cart->total()+200);?></i></td>
										</tr>
									</tbody>
								</table>
								<h4 class="widget-title">Payment Method</h4>
							
								<div class="form-group">
									<select class="default-select" id="payment_type">
										<option value="1">Online</option>
                                        <!--<option value="2">COD</option>-->
									</select>	
								</div>
							
								<div class="form-group">
									<button class="btn btn-primary btnhover" onclick ="order_place()" type="button">Place Order Now </button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- Product END -->
			</section>
			<!-- contact area End--> 
		</div>

        <script>
            function deleteAddress(id){
                var messageText  = "You want delete this address!";
     var confirmText =  'Yes, delete it!';
     var message  ="Shipping address delete Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('cart/delete_address')?>', 
                method: 'POST',
                data: {id},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }


  $("form#addAddress").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
        if(data.status==200) {
        //$('.modal').modal('hide');
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
                setTimeout(function(){
                    location.reload();
        }, 1000) 

        }else if(data.status==403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        }else{
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
        }
    },
    error: function(){} 
    });
});

function order_place(){
	var payment_type = $('#payment_type').val();
	var shipping_charge = <?=$shipping_charge?>;
	var address = <?=$totalAddress?> > 0  ? $('input[type=radio][name="addressID"]:checked').val() : 0;
	if($('input[type=radio][name="addressID"]:checked').length >  0){
		$.ajax({
       url: '<?=base_url('CcavRequestHandler')?>',
       type: 'POST',
	   data:{payment_type,shipping_charge,address},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
			window.location.href = '<?=base_url("make-payment")?>';
	   }
       },
       error: function(){} 
     });
	}else{
		toastr.error('please choose address');
	}
}
</script>