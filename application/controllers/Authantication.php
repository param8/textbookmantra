<?php 
class Authantication extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
	}

	/* 
	* It only redirects to the manage category page
	* It passes the total product, total paid orders, total users, and total stores information
	into the frontend.
	*/
	public function index()
	{	$data['page_title'] = 'Authantication';
		$data['siteinfo'] = $this->siteinfo();
		$this->load->view('layout/head',$data);
		//$this->load->view('layout/footer');
		$this->load->view('login');
		//$this->load->view('layout/footer');
	}

	public function login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$type = 'User';
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password,$type);

		if($login){
			echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home', 'refresh');
	}


	public function register(){
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$mobile = $this->input->post('mobile');
		$address= $this->input->post('address');
		$pincode = $this->input->post('pincode');
		$state = $this->input->post('state');
		$city = $this->input->post('city');

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			exit();
		}
		$checkEmail = $this->User_model->get_user(array('email'=>$email));
		if($checkEmail->num_rows() > 0){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		if(empty($mobile)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}
		if(empty($pincode)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your pincode']); 	
			exit();
		}
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
			exit();
		}
		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
			exit();
		}
		$uuid = uniqid();
		$data = array(
			'u_id'                =>$uuid,
			'name'                => $name,
			'email'               => $email,
			'password'            => md5($password),
			'mobile'              => $mobile ,
			'address'             => $address,
			'pin_code'            => $pincode,
			'state'               => $state,
			'city'                => $city,
			'type'                => 'User',
		);
		$register = $this->Auth_model->register($data);

		if($register){
			echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
		}
	}
//   Admin Cradinciales

public function adminIndex()
{	
    $this->logged_in();
    $data['page_title'] = 'Login';
	$data['siteinfo'] = $this->siteinfo();
	$this->load->view('admin/layout/login_head',$data);
	$this->load->view('admin/login');
	$this->load->view('admin/layout/login_footer');
}

public function adminLogin(){
	$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	$type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password,$type);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect('login', 'refresh');
}
	
	
}