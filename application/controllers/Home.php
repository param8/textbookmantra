<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->not_logged_in();
		$this->load->model('Common_model');
		$this->load->model('Product_model');
		$this->load->model('Slider_model');
		$this->load->model('Wishlist_model');
		$this->load->model('School_class_model');
		$this->load->model('Class_item_model');
    $this->load->library('pagination');
	
	}

	public function index(){

		$data['page_title'] = 'Home';
		$data['totalwishlist'] = $this->totalwishlist();
		//$data['states'] = $this->Common_model->get_states();
		$data['cities'] = $this->Common_model->get_cities();
		$data['schools'] = $this->schools();
		$data['sliders'] = $this->Slider_model->get_all_slider();
		$data['about_info'] = $this->aboutinfo();
		$this->website_template('home',$data);
	}

  public function book_set_combo(){
    $data['page_title'] = 'Book Set Combo';
    $data['schools'] = $this->schools();
    $this->website_template('book_set_combo',$data);
  }
  public function loose_books_schools(){
    $data['page_title'] = 'Loose Books';
    $data['schools'] = $this->schools();
    $this->website_template('loose_books_schools',$data);
  }

  public function LooseBooks(){
	$data['page_title'] = 'Loose Books';
		$classID = base64_decode($this->uri->segment(3));
		//print_r($classID);die;
		$data['class'] = $this->School_class_model->get_class_byID(array('id'=>$classID));
		$data['school'] = $this->school_detail(array('school_details.id'=>$data['class']->s_id));
		//print_r($class);die;
		$search_text = "";
		if($this->input->post('submit') != NULL ){
		$search_text = $this->input->post('search');
		$this->session->set_userdata(array("search"=>$search_text));
	}
		$data['loose_books'] = $this->Common_model->get_loose_books(array('school_class_items.s_id'=>$data['school']->id,'school_class_items.sc_id'=>$data['class']->id));
		$this->website_template('loose_books',$data);
  }
  public function loose_books(){
	
    $data['page_title'] = 'Loose Books';
    //$segment = $this->uri->segment(2)==' ' ? 'loose-books/0' : 'loose-books'; 
    $config = array();
    $config["base_url"] = base_url('home/loose_books');
    $config["total_rows"] =  $this->Class_item_model->get_count_lossebook();
	$config["full_tag_open"] = "<ul class='pagination style-1 p-t20'>";
	$config["full_tag_close"] = "</ul>";
	$config["prev_tag_open"] = "<li class='page-item page-link'>";
	$config["prev_tag_close"] = "</li>";
	$config["next_tag_open"] = "<li class='page-item page-link'>";
	$config["next_tag_close"] = "</li>";
	$config["last_tag_open"] = "<li class='page-item page-link next'>";
	$config["last_tag_close"] = "</li>";
	$config["first_tag_open"] = "<li class='page-item page-link prev'>";
	$config["first_tag_close"] = "</li>";
	$config["num_tag_open"] = "<li class='page-item page-link'>";
	$config["num_tag_close"] = "</li>";
	$config["cur_tag_open"] = "<li class='page-item page-link active'>";
	$config["cur_tag_close"] = "</li>";

	// Search text
	$search_text = "";
	if($this->input->post('submit') != NULL ){
		$search_text = $this->input->post('search');
		$this->session->set_userdata(array("search"=>$search_text));
	}

    //print_r($config["total_rows"]);die;
    $config["per_page"] = 12;
    $config["uri_segment"] = 3;
    $this->pagination->initialize($config);
    $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
    $data["links"] = $this->pagination->create_links();
    $data['total_rows'] = $config["total_rows"] ;
    $data['per_page'] = $page;
    $data['loose_books'] = $this->Class_item_model->get_all_loosebook($config["per_page"], $page);
    $this->website_template('loose_books',$data);
    
  }

  public function stationary(){
	$data['page_title'] = 'Stationary';
    //$segment = $this->uri->segment(2)==' ' ? 'loose-books/0' : 'loose-books'; 
    $config = array();
    $config["base_url"] = base_url('home/stationary');
    $config["total_rows"] =  $this->Class_item_model->get_count_stationary();
	$config["full_tag_open"] = "<ul class='pagination style-1 p-t20'>";
	$config["full_tag_close"] = "</ul>";
	$config["prev_tag_open"] = "<li class='page-item page-link'>";
	$config["prev_tag_close"] = "</li>";
	$config["next_tag_open"] = "<li class='page-item page-link'>";
	$config["next_tag_close"] = "</li>";
	$config["last_tag_open"] = "<li class='page-item page-link next'>";
	$config["last_tag_close"] = "</li>";
	$config["first_tag_open"] = "<li class='page-item page-link prev'>";
	$config["first_tag_close"] = "</li>";
	$config["num_tag_open"] = "<li class='page-item page-link'>";
	$config["num_tag_close"] = "</li>";
	$config["cur_tag_open"] = "<li class='page-item page-link active'>";
	$config["cur_tag_close"] = "</li>";

	// Search text
	$search_text = "";
	if($this->input->post('submit') != NULL ){
		$search_text = $this->input->post('search');
		$this->session->set_userdata(array("search"=>$search_text));
	}

    //print_r($config["total_rows"]);die;
    $config["per_page"] = 12;
    $config["uri_segment"] = 3;
    $this->pagination->initialize($config);
    $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
    $data["links"] = $this->pagination->create_links();
    $data['total_rows'] = $config["total_rows"] ;
    $data['per_page'] = $page;
    $data['stationary'] = $this->Class_item_model->get_all_stationary($config["per_page"], $page);
    $this->website_template('stationary',$data);
  }

  public function resetSearchFilter(){
	$this->session->unset_userdata('search');
  }
	public function about(){
		$data['totalwishlist'] = $this->totalwishlist();
		$data['about_info'] = $this->Common_model->get_about();
		$data['states'] = $this->Common_model->get_states();
		$data['cities'] = $this->Common_model->get_cities();
		$data['schools'] = $this->schools();
		$data['page_title'] = 'About us';
		$this->website_template('about',$data);
	}

	public function contact(){
		$data['totalwishlist'] = $this->totalwishlist();
		$data['states'] = $this->Common_model->get_states();
		$data['cities'] = $this->Common_model->get_cities();
		$data['about_info'] = $this->aboutinfo();
		$data['schools'] = $this->schools();
		$data['page_title'] = 'Contact us';
		$this->website_template('contact',$data);
	}

	public function login(){
		$data['page_title'] = 'Log In';
		$this->website_template('login',$data);
	}

	public function product_detail()
	{
		$proId = base64_decode($this->uri->segment(2));
		$data['states'] = $this->Common_model->get_states();
		$data['cities'] = $this->Common_model->get_cities();
		$data['totalwishlist'] = $this->totalwishlist();
		$data['about_info'] = $this->aboutinfo();
		$data['schools'] = $this->schools();
		$data['bookSet'] = $this->Product_model->get_product(array('id'=>$proId));
		$data['items'] = $this->Product_model->get_items($data['bookSet']->items);
		$data['page_title'] = 'Product Detail';
		//echo "<pre>";print_r($data);echo "</pre>";die;
		$this->website_template('product_detail',$data);
	
	}
	

  public function classes(){
	$schoolID = base64_decode($this->uri->segment(2));
	$data['totalwishlist'] = $this->totalwishlist();
    $data['page_title'] = 'Classes';
	$data['classes'] = $this->School_class_model->get_class(array('s_id'=>$schoolID));
	$data['school'] = $this->school_detail(array('school_details.id'=>$schoolID));
	$data['states'] = $this->Common_model->get_states();
	$data['cities'] = $this->Common_model->get_cities();
	$data['about_info'] = $this->aboutinfo();
    $data['siteinfo'] = $this->siteinfo();
	$data['schools'] = $this->schools();
	$this->load->view('layout/head',$data);
	$this->load->view('layout/header');
	$this->load->view('layout/side-filter');
	$this->load->view('classes');
	$this->load->view('layout/school_category');
	$this->load->view('layout/footer');

  }

  public function book_classes(){
	$schoolID = base64_decode($this->uri->segment(2));
	$data['totalwishlist'] = $this->totalwishlist();
    $data['page_title'] = 'Classes';
	$data['classes'] = $this->School_class_model->get_class(array('s_id'=>$schoolID));
	$data['school'] = $this->school_detail(array('school_details.id'=>$schoolID));
	$data['states'] = $this->Common_model->get_states();
	$data['cities'] = $this->Common_model->get_cities();
	$data['about_info'] = $this->aboutinfo();
    $data['siteinfo'] = $this->siteinfo();
	$data['schools'] = $this->schools();
	$this->load->view('layout/head',$data);
	$this->load->view('layout/header');
	$this->load->view('loose_books_classes');
	$this->load->view('layout/school_category');
	$this->load->view('layout/footer');

  }

  public function book_sets(){
    $classID = base64_decode($this->uri->segment(2));
	$data['totalwishlist'] = $this->totalwishlist();
    $data['page_title'] = 'Books';
	$data['class'] = $this->School_class_model->get_class_byID(array('id'=>$classID));
	$data['school'] = $this->school_detail(array('school_details.id'=>$data['class']->s_id));
	$data['books'] = $this->Common_model->get_book_sets(array('book_set.s_id'=>$data['school']->id,'book_set.sc_id'=>$data['class']->id));
	$data['states'] = $this->Common_model->get_states();
	$data['cities'] = $this->Common_model->get_cities();
	$data['siteinfo'] = $this->siteinfo();
	$data['about_info'] = $this->aboutinfo();
	$data['schools'] = $this->schools();
	$this->load->view('layout/head',$data);
	$this->load->view('layout/header');
	$this->load->view('layout/side-filter');
	$this->load->view('book_sets');
	$this->load->view('layout/school_category');
	$this->load->view('layout/footer');
  }
	
}
