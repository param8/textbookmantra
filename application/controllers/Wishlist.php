<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Wishlist extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->not_logged_in();
    $this->load->model('Common_model');
		$this->load->model('Wishlist_model');
		$this->load->library('cart');
  }
  public function index()
  {
    $data['page_title'] = 'Wishlist';
    $data['WishItems'] = $this->Wishlist_model->getwishlist();
    $data['totalwishlist'] = $this->totalwishlist();
    $data['siteinfo'] = $this->siteinfo();
    $data['schools'] = $this->schools();
    $data['WishItem'] = $this->Wishlist_model->get_products();
    $this->website_template('wishlist',$data);

  }
  public function wishstore_cart(){
    $productID = $this->input->post('productID');
    $userId = $this->session->userdata('id');
    $data = array(
       'product_id' => $productID,
       'user_id'    => $userId,
    );
    
    $store = $this->Wishlist_model->store_wishList($data);
    $totalwishlist = $this->Wishlist_model->getwishlist();
   if($store){
    echo json_encode(['status'=>200, 'message'=>'Wishlist item Added successsfully', 'totalCount'=> count($totalwishlist)]);
   }
 }
 public function removeWishItem(){
  $id =  $this->input->post('id');
  $data = array(
      'id'   => $id,
      'qty'     => 0
  );
    $delete= $this->Wishlist_model->delete_wishList($id);
    if($delete){
      echo json_encode(['status'=>200, 'message'=>'Wishlist item removed successfully']);
    }else{
      echo json_encode(['status'=>403,'message'=>'Wishlist delete failed']);
    }
}



















}
