<?php 
class CcavRequestHandler extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_logged_in();
    $this->load->model('Cart_model');
    $this->load->library('ccavenue');
	}

    public function index(){
       // $gst = !empty($this->input->post('gst')) ? $this->input->post('gst') : 0;
        $amount = $this->cart->total();
        $totalAmount = $amount + $this->input->post('shipping_charge');
        $payment_type = $this->input->post('payment_type');
        $tin =strtotime("now");
        $this->db->order_by('id',desc);
        $order = $this->db->get('orders')->row();
        if($order){
            $id_no = $order->id + 1;
            $orderID = 'TBM#'.$id_no;
        }else{
            $orderID = 'TBM#1';
        }
        
        //echo  $orderID;die;
        
        $address = $this->Cart_model->get_billing_address(array('shipping_details.id' =>$this->input->post('address')));
        $order_detail = array(
          'user_id'=>$this->session->userdata('id'),
          'order_id'=>$orderID,
          'ship_id'=>'',
          'set_qty'=>$this->cart->total_items(),
          'shipping_charge'=>$this->input->post('shipping_charge'),
          'net_amount'=>$amount,
          'gst'=>'',
          'total_amount'=>$totalAmount,
          'type'=>$payment_type,
          'payment_status'=>'',
          'payment_detail'=>'',
          'address_id'=>$this->input->post('address'),
        );
    
         $store_order = $this->Cart_model->store_order($order_detail);
         if($store_order){
          $cartItems=$this->cart->contents();                           
            foreach($cartItems as $item){
            $item_order = array(
             'orderID' => $store_order,
             'order_no' => $orderID,
             'book_set_id' => $item['id'],
             'type' => '',
             'mrp' => $item['price'],
             'discount' => '',
             'amount' => $item['price'],
             'qty' => $item['qty'],
            );

            $store_item = $this->Cart_model->store_item($item_order);
          }
        $data=array(
            'tid'=>$tin,
            'merchant_id'=>'712032',
            'order_id'=>$orderID,
            'amount'=>$totalAmount,
            'currency'=>'INR',
            'redirect_url'=>base_url('Payment_handler'),
            'cancel_url'=>base_url('Payment_handler'),
            'language'=>'EN',
            'delivery_name'=>$address->name,
            'delivery_address'=>$address->address,
            'delivery_city'=>$address->cityName,
            'delivery_state'=>$address->stateName,
            'delivery_zip'=>$address->pin_code,
            'delivery_country'=>'India',
            'delivery_tel'=>$address->mobile,
            'billing_name'=>$address->name,
            'billing_address'=>$address->address,
            'billing_city'=>$address->cityName,
            'billing_state'=>$address->stateName,
            'billing_zip'=>$address->pin_code,
            'billing_country'=>'India',
            'billing_tel'=>$address->mobile,
            'billing_email'=>$address->email
            );
            $merchant_data='';
            $working_key='4D51A19FEC0EB2AB2325D567BDF78FB2';//Shared by CCAVENUES
            $access_code='AVSK56IK79AS54KSSA';//Shared by CCAVENUES
            
            foreach ($data as $key => $value){
                $merchant_data.=$key.'='.urlencode($value).'&';
            }

            $encrypted_data=$this->ccavenue->encrypt($merchant_data,$working_key);// Method for encrypting the data.
            $encript_session =array(
                'data_enc' =>base64_encode($encrypted_data),
                'code' =>base64_encode($access_code)
            );
            $this->session->set_userdata($encript_session);
            echo json_encode(['status'=>200]);
          }else{
            echo json_encode(['status'=>403]);
          }
    }

    public function send(){
        $encrypted_data =base64_decode($this->session->userdata('data_enc'));
        $access_code = base64_decode($this->session->userdata('code'));
        ?>
        <form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 
        <?php
        echo "<input type=hidden name='encRequest' value=$encrypted_data>";
        echo "<input type=hidden name='access_code' value=$access_code>";
        ?>
        </form></center><script language='javascript'>document.redirect.submit();</script>
        <?php
    }


}