<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_handler extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Cart_model');
		$this->load->model('Auth_model');
        $this->load->library('ccavenue');
	}
	public function index()
	{
			
	$workingKey='4D51A19FEC0EB2AB2325D567BDF78FB2';		//Working Key should be provided here.
	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	$rcvdString=$this->ccavenue->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	$order_status="";
	$order_id= 0;
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);

    $payment_detail = array();
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		$payment_detail[] =  array(
			$information[0] => $information[1],
		);
		if($i==3){
			$order_status=$information[1];
		}

		if($i==0){
			$order_id=$information[1];
		}
	}
	$payment = json_encode($payment_detail);
	
	if($order_status==="Success" || $order_status=="Aborted" || $order_status=="Failure" || $order_status=="Initiated"){
		$payment_status = array(
        'payment_status' => $order_status,
		'payment_detail' => $payment,
		);
		$get_order = $this->Cart_model->get_order(array('order_id'=>$order_id));
        $update_order = $this->Cart_model->update_order($payment_status,array('order_id'=>$order_id));
	  $this->cart->destroy();
	  $this->session->unset_userdata('data_enc');
      $this->session->unset_userdata('code');
      $get_user = $this->Auth_model->payment_login($get_order->user_id);
	 // print_r($this->session->userdata());
	  redirect(base_url('confirmation-order/'.base64_encode($order_id).'/'.base64_encode($order_status)));
	}


    //print_r($information);
// 	if($order_status==="Success")
// 	{
// 		echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
		
// 	}
// 	else if($order_status==="Aborted")
// 	{
// 		echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
	
// 	}
// 	else if($order_status==="Failure")
// 	{
// 		echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
// 	}
// 	else
// 	{
// 		echo "<br>Security Error. Illegal access detected";
	
// 	}

	// echo "<br><br>";

	// echo "<table cellspacing=4 cellpadding=4>";
	// for($i = 0; $i < $dataSize; $i++) 
	// {
	// 	$information=explode('=',$decryptValues[$i]);
	//     	echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
	// }

	// echo "</table><br>";
	// echo "</center>";die;


//  print_r($encrypted_data);die;

	}



}