<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->not_logged_in();
    $this->load->model('Cart_model');
    $this->load->model('Wishlist_model');
    $this->load->model('Class_item_model');
    $this->load->model('Common_model');
  }

  public function index()
  {
    $data['page_title'] = 'Cart';
    $data['siteinfo'] = $this->siteinfo();
    $data['about_info'] = $this->aboutinfo();
    $data['totalwishlist'] = $this->totalwishlist(); 
    $data['schools'] = $this->schools(); 
    $this->website_template('cart',$data);
  }

  public function store_cart(){
    $productID = $this->input->post('productID');
    if($this->input->post('book_type')=='Loose Book'){
      $cartItem = $this->Cart_model->get_books($productID);
      //$itemname=$cartItem['title'];
      //$titlename= str_replace('(',"-0",$itemname);
      
      $title =$cartItem['title'];
      $items = 0;
      $price = $cartItem['special_price'];
    }else{
      $cartItem = $this->Cart_model->get_product($productID);
      $title =$cartItem['title'];
      $items = $cartItem['items'];
      $price =  $cartItem['price'];
    }
     
      $data = array(
        'id'   => $productID,
        'name' => $title,
        'qty' => 1,
        'image' =>$cartItem['image'],
        'price' =>intval($price),
        'items' =>$items,
     );
     //print_r($data);
     $cart = $this->cart->insert($data);
     //print_r($cart);die();
     //$cartItem=$this->cart->contents();
     echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
  }

  public function store_Book_cart(){
    $productID = $this->input->post('productID');
      $cartItem = $this->Cart_model->get_books($productID);
      //print_r($cartItem);die();
    
     $data = array(
        'id'   => $productID,
        'name' => $cartItem['title'],
        'qty' => 1,
        'image' =>$cartItem['image'],
        'price' =>$cartItem['price'],
        'items' =>$cartItem['items']
     );

     $cart = $this->cart->insert($data);
     //$cartItem=$this->cart->contents();
     echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
  }



  public function update_cart(){
   $qty =  $this->input->post('qty');
   $rowid =  $this->input->post('rowid');
   $data = array(
    'rowid' => $rowid,
    'qty'   => $qty,
   );
   $update=$this->cart->update($data);
   echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
  }

  public function removeCartItem() {   
    $rowid =  $this->input->post('rowid');
    $data = array(
        'rowid'   => $rowid,
        'qty'     => 0
    );
    echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
    $update=$this->cart->update($data);
}

public function store_productDetail_cart(){
    $productID = $this->input->post('bookSetID');
    $price = $this->input->post('totalAmount');
    if(empty($this->input->post('item'))){
      echo json_encode(['status'=>403, 'message'=> 'Please choose atleast one item']);
      exit();
    }
    $getItems = $this->cart->contents();
    $booksetId = array();
    $rowID = "";
    foreach($getItems as $getItem){
      $booksetId[]= $getItem['id'];
      $rowID = $getItem['rowid'];
    }
    if(in_array($productID,$booksetId)){
      $dataUpdate = array(
        'rowid'   => $rowID,
        'qty'     => 0
        );
        $this->cart->update($dataUpdate);
    }

     $items  = implode(',',$this->input->post('item'));
     $cartItem = $this->Cart_model->get_product($productID);
     $data = array(
        'id'   => $productID,
        'name' => $cartItem['title'],
        'qty'  => 1,
        'image' =>$cartItem['image'],
        'price' =>$price,
        'items' =>$items
     );

     $cart = $this->cart->insert($data);
    //  $cartItem=$this->cart->contents();
    //  print_r($cartItem);
     if($cart){
       echo json_encode(['status'=>200,'message'=>'Book set added  successfully','totalCount'=> $this->cart->total_items()]);
     }
}

public function cart_detail(){
  $rowid = $this->input->post('rowid');
  $cartItems=$this->cart->contents();
  $cartItem = $cartItems[$rowid];
  $itemID = explode(',',$cartItem['items']);
 
  $items=  $this->Class_item_model->get_class_item_byID($itemID);
 
  ?>
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"><?=$cartItem['name']?></h5>
     
    </div>
    <div class="modal-body">
      <div class="table-responsive">
            <table class="table table-striped check-tbl">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Item name</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($items as $item){?>
                <tr>
                  <td class="product-item-img"><img src="<?= base_url($item['image'])?>" alt=""></td>
                  <td class="product-item-name"><?= $item['title']?></td>
                  <td class="product-item-price"><i class="fa fa-inr"></i><?= intval($item['special_price'])?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
    </div>
    <?php 
}

 public function checkout(){
  $data['page_title'] = 'Checkout';
  $data['siteinfo'] = $this->siteinfo();
  $data['about_info'] = $this->aboutinfo();
  $data['totalwishlist'] = $this->totalwishlist(); 
  $data['schools'] = $this->schools(); 
  $data['pincodes'] = $this->Common_model->get_pincode();
  $data['states'] = $this->Common_model->get_states();
  $data['addresses'] = $this->Cart_model->get_shipping_address();
  $this->website_template('checkout',$data);
 }

 public function delete_address(){
  $id = $this->input->post('id');
  $data = array(
    'status' => 0
  );
  $delete = $this->Cart_model->update_address($data,$id);
  if($delete)
		{
			echo json_encode(['status'=>200, 'message'=>'Delete shipping address successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
      exit();
		}
 }

 public function store_shippingAddress(){
  $name = $this->input->post('name');
  $email = $this->input->post('email');
  $mobile = $this->input->post('mobile');
  $address = $this->input->post('address');
  $street = $this->input->post('street');
  $pin_code = $this->input->post('pin_code');
  $state = $this->input->post('state');
  $city = $this->input->post('city');
  $userID = $this->session->userdata('id');
  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a name']);
      exit();
  }
  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a email']);
      exit();
  }
  if(empty($mobile)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a mobile']);
      exit();
  }
  if(empty($address)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a address']);
      exit();
  }
  if(empty($street)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a street']);
      exit();
  }
  if(empty($pin_code)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a pincode']);
      exit();
  }
  if(empty($state)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a state']);
      exit();
  }
  if(empty($city)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a city']);
      exit();
  }
 $data = array(
  'uid' => $userID,
  'name' => $name,
  'mobile' => $mobile,
  'email' => $email,
  'address' => $address,
  'street' => $street,
  'pin_code' => $pin_code,
  'city' => $city,
  'state' => $state,
  'create_date' => date('Y-m-d H:i:s'),
  'modify_date' => date('Y-m-d H:i:s'),
  'status' => 1,
 );

  $store = $this->Cart_model->store_shippingAddress($data);
  if($store)
  {
    echo json_encode(['status'=>200, 'message'=>'Shipping address add successfully!']);
        exit();
  }else{
    echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
    exit();
  }
 }


 public function confirmation_order(){
  $data['page_title'] = 'Cart';
  $data['order_id'] =base64_decode($this->uri->segment(2));
  $data['order_status'] =base64_decode($this->uri->segment(3));
  $data['siteinfo'] = $this->siteinfo();
  $data['about_info'] = $this->aboutinfo();
  $this->website_template('thankyou',$data);
 }

}