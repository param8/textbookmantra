<?php 
class Language extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->logged_in();
		$this->load->model('School_model');
		$this->load->model('School_class_model');
		$this->load->model('Language_model');
        $this->load->model('Common_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Language';
		$data['siteinfo'] = $this->siteinfo();
		$data['languages'] = $this->Language_model->get_all_languages();
		$this->admin_template('language',$data);
	}

	public function store(){
        $language_name = $this->input->post('language_name');
		// check if same language already exist
		$language_exist = $this->Language_model->is_language_exist($language_name);
		if($language_exist){
			echo json_encode(['status'=>403, 'message'=>'Language already exist!']);
				exit();
		}
		if(empty($language_name)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a language']);
      		exit();
		}
    	
		$data = array(
			'lang_uid' 		=> uniqid(),
			'language_name' => $language_name,	
			'create_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 1 
		);

		$add = $this->Language_model->add_language($data);
		if($add)
		{
			echo json_encode(['status'=>200, 'message'=>'Language added successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not added. please try again!']);
      		exit();
		}
	}

    public function edit()
    {
        $lang_uid       = $this->input->post('lang_uid');
        $language_name  = $this->input->post('language_name');

        // check if same language already exist
		$language_exist = $this->Language_model->is_language_exist($language_name, $lang_uid);
        if($language_exist){
			echo json_encode(['status'=>403, 'message'=>'Language already exist!']);
				exit();
		}

        if(empty($language_name)){
            echo json_encode(['status'=>403, 'message'=>'Please enter a language']);
                exit();
        }

        $data = array(
			'language_name' => $language_name,	
			'modify_date' 	=> date('Y-m-d H:i:s')
		);

        $update = $this->Language_model->update_language($data, $lang_uid);
		if($update)
		{
			echo json_encode(['status'=>200, 'message'=>'Language updated successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not updated. please try again!']);
      		exit();
		}

    }
    	
}