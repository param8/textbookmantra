<?php 
class Dashboard extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		
		$this->load->model('auth_model');
	}

	public function index()
	{	 
	    $this->logged_in();
	    $totalschool = $this->Common_model->get_dashboardData('school');
		$data['schools'] = $totalschool;
		//echo $dashboardcount;die();
		$totalclasses = $this->Common_model->get_dashboardData('class');
		//print_r($totalclasses);die;
		$data['classes'] = $totalclasses == ''?0:$totalclasses;
		$totalbooks = $this->Common_model->get_dashboardData('items');
		$data['books'] = $totalbooks == ''?0:$totalbooks;
		$totalbookset = $this->Common_model->get_dashboardData('bookSet');
		//echo $totalbookset;die();
		$data['booksets'] = $totalbookset==''?0:$totalbookset;
		//$data['']
		 $data['page_title'] = 'Dashboard';
		 $data['siteinfo'] = $this->siteinfo();
	     $this->admin_template('dashboard',$data);
	}

    public function addUID(){
        
        $users = $this->db->get('users')->result();
        foreach ($users as $user){
			$userPassword = $user->password;
			$this->db->where('id', $user->id);
			$this->db->update('users',array('password'=>md5($userPassword)));
        }
    }



	
}