<?php 
class Book_set extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_admin_logged_in();
    $this->logged_in();
		$this->load->model('School_model');
		$this->load->model('School_class_model');
    $this->load->model('Class_item_model');
		$this->load->model('Language_model');
     $this->load->model('Common_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Book Set';
    $data['siteinfo'] = $this->siteinfo();
    $data['schools'] = $this->School_model->get_all_schools();
		$data['bookSets'] = $this->Common_model->get_all_book_set();
    $this->admin_template('book_set',$data);
	}

	public function bookSetCreateForm(){
		$classID =!empty($this->input->post('classID')) ? $this->input->post('classID') : 0;
    $class = $this->School_class_model->get_class_byID(array('id'=>$classID));
    $schoolID = !empty($class->s_id) ? $class->s_id : 0 ;
		$items = $this->Class_item_model->get_school_class_wise_item(array('school_class_items.s_id'=>$schoolID,'school_class_items.sc_id'=>$classID));
		$allItems =array();
    if(count($items)>0){
      if(!empty($this->input->post('bookSetID'))){
		$book_set = $this->Common_model->get_book_set(array('book_set.id'=>$this->input->post('bookSetID')));
		
		if($book_set){
		  $allItems = explode(',',$book_set->items);
		}
   }
		//print_r($allItems);
		foreach(array_filter($items) as $item) {
      //echo $item->id;
		?>	
    <div> 
			 <label>
				<input type="checkbox" class="checkboxAmount checkboxshow" name="item[]" value="<?=$item->id?>" id="item<?=$item->id?>" <?php if(!empty($this->input->post('bookSetID'))){ echo in_array($item->id,$allItems) ? 'checked="checked"' : '';}?> onchange="get_total_amount()"  >
				<?=$item->title?> <span class="text-danger mr-5 font-weight-bold" > <i class="fa fa-rupee"></i><input type="text" value="<?=$item->special_price?>" id="itemAmount<?=$item->id?>" readonly style="border:none"></span></label> 
    </div>
		<?php
		}
    ?>
    <hr>
    <div>
      <span class="font-weight-bold">Total Amount:-</span>  <span class="text-danger mr-2"><i class="fa fa-rupee"></i><input type="text" class="totalAmount"  name="totalAmount" id="" readonly style="border:none"></span>
    </div>
    <?php
	  }else{
		echo '<div class="text-danger">Data not found</div>';
	  }
	  }


    public function bookSetCreateFormEdit(){
      $classID =!empty($this->input->post('classID')) ? $this->input->post('classID') : 0;
      $class = $this->School_class_model->get_class_byID(array('id'=>$classID));
      $schoolID = !empty($class->s_id) ? $class->s_id : 0 ;
      $items = $this->Class_item_model->get_school_class_wise_item(array('school_class_items.s_id'=>$schoolID,'school_class_items.sc_id'=>$classID));
      $allItems =array();
      if(!empty($this->input->post('bookSetID'))){
        $book_set = $this->Common_model->get_book_set(array('book_set.id'=>$this->input->post('bookSetID')));
        
        if($book_set){
          $allItems = explode(',',$book_set->items);
        }
       }
      if(count($items)>0){
   
      //print_r($allItems);
      foreach(array_filter($items) as $item) {
        //echo $item->id;
      ?>	
      <div> 
         <label>
          <input type="checkbox" class="checkboxAmountEdit checkboxshow" name="item[]" value="<?=$item->id?>" id="itemEdit<?=$item->id?>" <?php if(!empty($this->input->post('bookSetID'))){ echo in_array($item->id,$allItems) ? 'checked="checked"' : '';}?> onchange="get_total_amountEdit()" >
          <?=$item->title?> <span class="text-danger mr-5 font-weight-bold" > <i class="fa fa-rupee"></i><input type="text" value="<?=$item->special_price?>" id="itemAmount<?=$item->id?>" readonly style="border:none"></span></label> 
      </div>
      <?php
      }
      ?>
      <hr>
      <div>
        <span class="font-weight-bold">Total Amount:-</span>  <span class="text-danger mr-2"><i class="fa fa-rupee"></i><input type="text" class="totalAmount"  name="totalAmount" id="" readonly style="border:none"></span>
      </div>
      <?php
      }else{
      echo '<div class="text-danger">Data not found</div>';
      }
      }

	public function store(){
		$title = $this->input->post('title');
		$school = $this->input->post('school');
		$class = $this->input->post('class');
		$item = $this->input->post('item');
		$price = $this->input->post('totalAmount');

		if(empty($title)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a title!']);
      exit();
		}
    	if(empty($school)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a school']);
      exit();
		}
    if(empty($class)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a class']);
      exit();
		}

    if(empty($item)){
		  echo json_encode(['status'=>403, 'message'=>'Please check at least one book']);
      exit();
		}

		if($_FILES['image']['name'] != '')
		{
			$config['upload_path']          = 'uploads/book_set';
			$config['allowed_types']        = 'jpg|jpeg|png|gif';
			$config['max_size']             = 2000;
			$config['max_width']            = 2048;
			$config['max_height']           = 2048;
			$config['file_name']			= time() . $_FILES['image']['name'];

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('image'))
			{
				echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
				exit();
			}
			else
			{
				$image = 'uploads/book_set/'.$this->upload->data('file_name');
			}
		}else{
			$image = 'public/website/images/dummy_image.jpg';
		}
		
    $set_item = implode(',',$item);

		$data = array(
			'book_set_uid' => uniqid(),
			's_id'	  		 => $school,
			'sc_id' 	     => $class,
			'items'			   => $set_item,
			'title' 		   => $title,
			'price'			   =>	$price,	
			'image' 	     => $image,
		);
   $checkbooksettitle = $this->Common_model->check_book_set_title($title,$school,$class);
   if($checkbooksettitle)
   {
    echo json_encode(['status'=>403, 'message'=>'Book set already exits!']);
    exit();
   }
		$store = $this->Common_model->store_book_set($data);
		if($store)
		{
			echo json_encode(['status'=>200, 'message'=>'Book set added successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not added. please try again!']);
      		exit();
		}
  
	}

  public function bookSetEditForm(){
    $bookSetID =  $this->input->post('bookSetID');
    $schools = $this->School_model->get_all_schools();
		$book_set = $this->Common_model->get_book_set(array('book_set.id'=>$bookSetID));
    
    $set_ids= explode(',',$book_set->items);
    //var_dump($set_ids);
    ?>
    <input type="hidden" name="bookSetID" id="bookSetID" value="<?=$bookSetID?>">
		<div class="form-group">
            <label for="title" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="title" name="title" value="<?=$book_set->title?>" placeholder="Title">
          </div>

          <div class="form-group">
            <label for="image" class="col-form-label">Image:</label>
            <input type="file" class="form-control" id="image" name="image" >
          </div>
          <div class="form-group">
            <label for="school" class="col-form-label">School Name:</label>
            <select class="form-control school_id" name="school" id="school" onchange="getClasses(this.value)" disabled>
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>" <?=$book_set->s_id == $school->id ? 'selected' : ''?>><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="class" class="col-form-label">Class:</label>
            <select class="form-control class" name="class" id="class" onchange="getItemsEdit(this.value)" disabled>
              <option value="<?=$book_set->sc_id?>"><?=$book_set->className?></option>
            </select>
          </div>
          <div class="class_wise_itemEdit" >
            </div>
    <?php
	  }

    public function update(){
      $bookSetID =  $this->input->post('bookSetID');
      $title = $this->input->post('title');
      $school = $this->input->post('school');
      $class = $this->input->post('class');
      $item = $this->input->post('item');
      $price = $this->input->post('totalAmount');
     // print_r($item);die;
      if(empty($title)){
        echo json_encode(['status'=>403, 'message'=>'Please enter a title!']);
        exit();
      }
  
      if(empty($item)){
        echo json_encode(['status'=>403, 'message'=>'Please check at least one book']);
        exit();
      }

      $bookSet = $this->Common_model->get_book_set(array('book_set.id'=>$bookSetID));
      $this->load->library('upload');
      if(!empty($_FILES['image']['name'])){
      $config = array(
        'upload_path' 	=> 'uploads/book_set',
        'file_name' 	=> time() . $_FILES['image']['name'],
        'allowed_types' => 'jpg|jpeg|png|gif',
        'max_size' 		=> '10000000',
      );
          $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]);
            exit();
        }
        else
        {
           $image = 'uploads/book_set/'.$this->upload->data('file_name');
        }

      // if($_FILES['image']['name'] != '')
      // {
      //   $config['upload_path']          = 'uploads/book_set';
      //   $config['allowed_types']        = 'jpg|jpeg|png|gif';
      //   $config['max_size']             = 2000;
      //   $config['max_width']            = 2048;
      //   $config['max_height']           = 2048;
      //   $config['file_name']			= time() . $_FILES['image']['name'];
  
      //   $this->load->library('upload', $config);
  
      //   if ( ! $this->upload->do_upload('image'))
      //   {
      //     echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
      //     exit();
      //   }
      //   else
      //   {
      //      $image = 'public/website/images/dummy_image.jpg';
      //   }
       }
      
      elseif(!empty($bookSet->image)){
        $image = $bookSet->image;
       // echo $image;die();
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
      
      //$set_item = implode(',',$item);
  
      $data = array(
        'items'        => implode(',',$item),
        'title' 		   => $title,
        'price'			   =>	$price,	
        'image' 	     => $image,
      );
      $checkbooksettitle = $this->Common_model->check_book_set_title($title,$school,$class,$bookSetID);
      if($checkbooksettitle)
      {
       echo json_encode(['status'=>403, 'message'=>'Book set already exits!']);
       exit();
      }
      $update = $this->Common_model->update_book_set($data,array('id'=>$bookSetID));
      if($update)
      {
        echo json_encode(['status'=>200, 'message'=>'Book set added successfully!']);
            exit();
      }else{
        echo json_encode(['status'=>403, 'message'=>'Record not added. please try again!']);
            exit();
      }
    }

    public function delete(){
      $uid = $this->input->post('uid');
      $data = array(
        'status' => 0
      );
      $update = $this->Common_model->update_book_set($data,array('book_set_uid'=>$uid));
      if($update)
      {
        echo json_encode(['status'=>200, 'message'=>'Book set delete successfully!']);
            exit();
      }else{
        echo json_encode(['status'=>403, 'message'=>'Some thing went wrong. please try again!']);
            exit();
      }
    }
	
}