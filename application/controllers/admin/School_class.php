<?php 
class School_class extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_admin_logged_in();
    $this->logged_in();
		$this->load->model('School_model');
		$this->load->model('School_class_model');
		$this->load->model('Class_item_model');
		$this->load->model('Language_model');
		$this->load->model('Class_item_model');
        $this->load->model('Common_model');
		$this->load->library('csvimport');
	}

	public function index()
	{	
		$data['page_title'] = 'Classes';
    $data['siteinfo'] = $this->siteinfo();
		$data['schools'] = $this->School_model->get_all_schools();
		$data['languages'] = $this->Language_model->get_all_languages();
		$data['school_classes'] = $this->School_class_model->get_all_school_class();
		//print_r($data);die;
    $this->admin_template('school_class',$data);
	
	}

	public function store(){
		$s_id = $this->input->post('s_id');
		$language_id = $this->input->post('language_id');
		$classname = $this->input->post('classname');
		$remarks = $this->input->post('remarks');
		// check if same class already exist
		$class_exist = $this->School_class_model->is_class_exist($s_id, $language_id, $classname);
		if($class_exist){
			echo json_encode(['status'=>403, 'message'=>'Class already exist!']);
				exit();
		}
		if(empty($s_id)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a school']);
      		exit();
		}
    	if(empty($language_id)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a language']);
      		exit();
		}
    	if(empty($classname)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a class name']);
      		exit();
		}
		$this->load->library('upload', $config);
		if($_FILES['image']['name'] != '')
		{
			$config['upload_path']          = 'uploads/classes';
			$config['allowed_types']        = 'jpg|jpeg|png|gif';
			$config['max_size']             = 2000;
			$config['max_width']            = 2048;
			$config['max_height']           = 2048;
			$config['file_name']			= time() . $_FILES['image']['name'];

			
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('image'))
			{
				echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
				exit();
			}
			else
			{
				$image = 'uploads/classes/'.$this->upload->data('file_name');
			}
		}else{
			$image = 'public/website/images/dummy_image.jpg';
		}
		

		$data = array(
			'sc_u_id' 		=> uniqid(),
			's_id'	  		=> $s_id,
			'language' 		=> $language_id,
			'class'			=> $classname,
			'remarks' 		=> $remarks,
			'image'			=>	$image,	
			'create_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 1 
		);

		$add = $this->School_class_model->store_class($data);
		if($add)
		{
			echo json_encode(['status'=>200, 'message'=>'School class added successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not added. please try again!']);
      		exit();
		}
	}

	public function importClass(){
			$uniqid = uniqid();
			$s_id = $this->input->post('s_id');
			$school_image = $this->School_model->get_schoolImageById($s_id);
			//$Img= $school_image->image;
			//print_r($Img);die;
			$language_id = $this->input->post('language_id');
			if (isset($_FILES["importFile"])) {
			  $config['upload_path']   = "uploads/importClasses/";
			  $config['allowed_types'] = 'text/plain|text/csv|csv';
			  $config['max_size']      = '2048';
			  $config['file_name']     = $uniqid.$_FILES["importFile"]['name'];
			  $config['overwrite']     = TRUE;
			  $this->load->library('upload', $config);
			  $this->upload->initialize($config);
			  if (!$this->upload->do_upload("importFile")) {
				echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			   exit();
			  } else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/importClasses/'.$file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
				  $csv_array = $this->csvimport->get_array($file_path);
				  // check if any duplicate record exist in system
				  $duplicate_found = false;
				  $err_msg = '';
				  //print_r($csv_array);die;
				  foreach ($csv_array as $key => $row) {
					$sc_u_id = uniqid();
					  $insert_data = array(
					  'sc_u_id' 		=> $sc_u_id,
						's_id'	  		=> $s_id,
						'language' 		=> $language_id,
						'class'			=> $row['classname'],
						'remarks' 		=> $row['remarks'],
						'image'			=>	$school_image->image,
						'create_date' 	=> date('Y-m-d H:i:s'),
						'status' 		=> 1 
					);
					$insert_id = $this->School_class_model->store_class($insert_data);   
				} 
				  echo json_encode(['status'=>200, 'message'=>'Upload Items Successfully']);
				  exit();
			  
			  }else{
				echo json_encode(['status'=>403, 'message'=>'Please Upload Correct CSV files']);
			   exit();
			  }
			} 
		  }else{
			echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			exit();
		  }
	}

	public function editForm(){
		$uid = $this->input->post('uid');
		$school_class = $this->School_class_model->get_school_class(array('sc_u_id'=>$uid));
		//print_r ($school_class);
		$schools = $this->School_model->get_all_schools();
		$languages = $this->Language_model->get_all_languages();
		?>
			<div class="form-group">
				<label for="s_id" class="col-form-label">School Name :</label>
				<select class="form-control school_id" name="s_id" id="s_id">
				<option value="">Select School</option>
				<?php foreach($schools as $school){?>
					<option value="<?=$school->id?>" <?=($school_class->s_id==$school->id)?'selected':'';?>><?=$school->name?></option>
				<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="language_id" class="col-form-label">Language:</label>
				<select class="form-control language_id" name="language_id" id="language_id">
				<option value="">Select Language</option>
				<?php foreach($languages as $language){?>
					<option value="<?=$language->id?>" <?=($school_class->language==$language->id)?'selected':'';?>><?=$language->language_name?></option>
				<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="classname" class="col-form-label">Class Name:</label>
				<input type="text" class="form-control" name="classname" id="classname" value="<?=$school_class->class;?>">
			</div>
			<div class="form-group">
				<label for="remarks" class="col-form-label">Remarks:</label>
				<textarea  class="form-control" name="remarks" id="remarks"><?=$school_class->remarks;?></textarea>
			</div>
			<div class="form-group">
				<label for="image" class="col-form-label">Class Image:</label>
				<input type="file" class="form-control" name="image" id="image">
			</div>
			<div class="form-group">
				<label for="status" class="col-form-label">Status:</label>
				<select class="form-control" name="status" id="status">
				<option value="">Select Status</option>
					<option value="1" <?=($school_class->status==1)?'selected':'';?>>Active</option>
					<option value="0" <?=($school_class->status==0)?'selected':'';?>>De-active</option>				
				</select>
			</div>
			<div class="form-group">
				<img src="<?=$school_class->image != "" ? base_url($school_class->image) : '' ;?>" width="100" height="100">
			</div>
			<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
		<?php
	  }
	
	  public function update(){
		$uid = $this->input->post('uid');
		$school_class = $this->School_class_model->get_school_class(array('sc_u_id'=>$uid));		
		$s_id = $this->input->post('s_id');
		$language_id = $this->input->post('language_id');
		$classname = $this->input->post('classname');
		$remarks = $this->input->post('remarks');
		$status = $this->input->post('status');
		// check if same class already exist
		$class_exist = $this->School_class_model->is_class_exist($s_id, $language_id, $classname, $uid);
		if($class_exist){
			echo json_encode(['status'=>403, 'message'=>'Class already exist!']);
				exit();
		}
		if(empty($s_id)){
		echo json_encode(['status'=>403, 'message'=>'Please select a school']);
			exit();
		}
		if(empty($language_id)){
		echo json_encode(['status'=>403, 'message'=>'Please select a language']);
			exit();
		}
		if(empty($classname)){
		echo json_encode(['status'=>403, 'message'=>'Please enter a class name']);
			exit();
		}

		// if($_FILES['image']['name'] != '')
		// {
		// 	$config['upload_path']          = 'uploads/classes';
		// 	$config['allowed_types']        = 'jpg|jpeg|png|gif|PNG|JPG';
		// 	$config['max_size']             = 2000;
		// 	$config['max_width']            = 2048;
		// 	$config['max_height']           = 2048;
		// 	$config['file_name']			= time() . str_replace(' ','_',$_FILES['image']['name']);

		// 	$this->load->library('upload', $config);

			$this->load->library('upload');
			if(!empty($_FILES['image']['name'])){
			$config = array(
			'upload_path' 	=> 'uploads/classes',
			'file_name' 	=> time() . str_replace(' ','_',$_FILES['image']['name']),
			'allowed_types' => 'jpg|jpeg|png|gif',
			'max_size' 		=> '10000000',
			);
				$this->upload->initialize($config);
	
			if ( ! $this->upload->do_upload('image'))
			{
				echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
				exit();
			}
			else
			{
				$image = 'uploads/classes/'.$this->upload->data('file_name');
			}
			}elseif(!empty($school_class->image)){
				$image = $school_class->image;
			}else{
				$image = 'public/website/images/dummy_image.jpg';
			}
		
		$data = array(
			's_id'	  		=> $s_id,
			'language' 		=> $language_id,
			'class'			=> $classname,
			'remarks' 		=> $remarks,
			'image'			=>	$image,	
			'modify_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> $status
		);
	
		$update = $this->School_class_model->update_class($data,array('sc_u_id'=>$uid));
		if($update){
		echo json_encode(['status'=>200, 'message'=>'School class update successfully!']);
		}else{
		echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
		}
		
	}
	
	public function delete(){
		$uid = $this->input->post('uid');
		$school_class = $this->School_class_model->get_school_class(array('sc_u_id'=>$uid)); 
		$data =  array(
		  'status' => '0'
		);
	
		$school_class_status = $this->School_class_model->update_class($data,array('sc_u_id'=>$uid));
		
		if($school_class_status){
			$itemdata =  array(
			  'status' => '0'
			);

		   $item_status =  $this->Class_item_model->update_class_items($itemdata,array('sc_id' => $school_class->id));
	  	}
	}
}