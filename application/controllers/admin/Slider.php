<?php 
class Slider extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->logged_in();
		$this->load->model('School_model');
		$this->load->model('Slider_model');
        $this->load->model('Common_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Slider';
		$data['siteinfo'] = $this->siteinfo();
		$data['schools'] = $this->School_model->get_all_schools();
		$data['sliders'] = $this->Slider_model->get_slider();
		$this->admin_template('slider',$data);
	}

	public function store(){
		$s_id = $this->input->post('s_id');
		$title = $this->input->post('title');
		$sub_title = $this->input->post('sub_title');
		$description = $this->input->post('description');
		$btn_link = $this->input->post('btn_link');
		$btn_text = $this->input->post('btn_text');
		
    	if(empty($title)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter title']);
      		exit();
		}
    	
		if($_FILES['image']['name'] != '')
		{
			$config['upload_path']          = 'public/uploads/';
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			$config['max_size']             = 20000;
			$config['max_width']            = 2048;
			$config['max_height']           = 2048;
			$config['file_name']			= time() . $_FILES['image']['name'];

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('image'))
			{
				echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
				exit();
			}
			else
			{
				$image = $this->upload->data('file_name');
			}
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please Upload Image!']);
			exit();
		}
		

		$data = array(
			'slider_uid' 	=> uniqid(),
			's_id'	  		=> $s_id,
			'title' 		=> $title,
			'sub_title' 	=> $sub_title,
			'description' 	=> $description,
			'btn_link'		=> $btn_link,
			'btn_text' 		=> $btn_text,
			'image'			=> $image,	
			'create_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 1 
		);

		$add = $this->Slider_model->add_slider($data);
		if($add)
		{
			echo json_encode(['status'=>200, 'message'=>'Slider added successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not added. please try again!']);
      		exit();
		}
	}

	public function editForm(){
		$uid = $this->input->post('uid');
		$slider = $this->Slider_model->get_sliderAdmin(array('slider_uid'=>$uid));
		//echo "<pre>";print_r($slider);echo "</pre>";die;
		$schools = $this->School_model->get_all_schools();
		//echo "<pre>";print_r($slider);echo "</pre>";die;
		foreach($slider as $sliders){
			
		?>
			<div class="form-group">
				<label for="s_id" class="col-form-label">School Name:</label>
				<select class="form-control school_id" name="s_id" id="s_id">
				<option value="">Select School</option>
				<?php foreach($schools as $school){?>
					<option value="<?=$school->id?>" <?=($sliders->s_id==$school->id)?'selected':'';?>><?=$school->name?></option>
				<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="title" class="col-form-label">Title:</label>
				<input type="text" class="form-control" name="title" id="title" value="<?=$sliders->title;?>">
			</div>
			<div class="form-group">
				<label for="sub_title" class="col-form-label">Sub Title:</label>
				<input type="text" class="form-control" name="sub_title" id="sub_title" value="<?=$sliders->sub_title;?>">
			</div>
			<div class="form-group">
				<label for="description" class="col-form-label">Description:</label>
				<textarea class="form-control" name="description" id="description"><?=$sliders->description;?></textarea>
			</div>
			<div class="form-group">
				<label for="btn_link" class="col-form-label">Button Link:</label>
				<input type="text" class="form-control" name="btn_link" id="btn_link" value="<?=$sliders->btn_link;?>">
			</div>
			<div class="form-group">
				<label for="btn_text" class="col-form-label">Button Text:</label>
				<input type="text" class="form-control" name="btn_text" id="btn_text" value="<?=$sliders->btn_text;?>">
			</div>
			<div class="form-group">
				<label for="image" class="col-form-label">Slider Image:</label>
				<input type="file" class="form-control" name="image" id="image">
			</div>
			<div class="form-group">
				<img src="<?=$sliders->image != "" ? base_url('public/uploads/'.$sliders->image) : '' ;?>" width="100" height="100">
			</div>
			<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
		<?php
	  }}

	  public function update(){
		$uid = $this->input->post('uid');
		$slider = $this->Slider_model->get_slider(array('slider_uid'=>$uid));		
		$s_id = $this->input->post('s_id');
		$title = $this->input->post('title');
		$sub_title = $this->input->post('sub_title');
		$description = $this->input->post('description');
		$btn_link = $this->input->post('btn_link');
		$btn_text = $this->input->post('btn_text');
	
		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter title']);
				exit();
		}

		if($_FILES['image']['name'] != '')
		{
			$config['upload_path']          = 'public/uploads/';
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			$config['max_size']             = 20000;
			$config['max_width']            = 2048;
			$config['max_height']           = 2048;
			$config['file_name']			= time() . str_replace(' ','_',$_FILES['image']['name']);

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('image'))
			{
				echo json_encode(['status'=>403, 'message'=>$this->upload->display_errors()]);
				exit();
			}
			else
			{
				$image = $this->upload->data('file_name');
			}
			}elseif(!empty($slider->image)){
				$image = $slider->image;
			}else{
				echo json_encode(['status'=>403, 'message'=>'Please Upload Image!']);
			    exit();
			}
		
		$data = array(
			's_id'	  		=> $s_id,
			'title' 		=> $title,
			'sub_title' 	=> $sub_title,
			'description' 	=> $description,
			'btn_link'		=> $btn_link,
			'btn_text' 		=> $btn_text,
			'image'			=> $image,	
			'modify_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 1 
		);
	
		$update = $this->Slider_model->update_slider($data,$uid);
		if($update){
		echo json_encode(['status'=>200, 'message'=>'Slider update successfully!']);
		}else{
		echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
		}
		
	}
	
	  public function delete(){
		$uid = $this->input->post('uid');
		$delete = $this->Slider_model->delete_slider($uid);
	  }
	  
	public function update_status()
	{
		$slider_id = $this->input->post('sliderid');
		$slider_status = ($this->input->post('slider_status')=='Active')?'1':'0';
		

        $sliderdata =  array(
          'status' => $slider_status
        );
        $update_status =  $this->Slider_model->update_slider_status($sliderdata,array('id'=>$slider_id));
	}
	
}