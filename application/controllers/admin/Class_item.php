<?php 
class Class_item extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_admin_logged_in();
    $this->logged_in();
	$this->load->model('Class_item_model');
    $this->load->model('School_class_model');
    $this->load->model('School_model'); 
    $this->load->model('Common_model');
    $this->load->library('csvimport');
	}

	public function index()
	{	
		$data['page_title'] = 'Class Item';
    $data['siteinfo'] = $this->siteinfo();
		$data['schools'] = $this->School_model->get_all_schools();
    $data['items'] = $this->Class_item_model->get_all_class_item();
    $this->admin_template('class_item',$data);
	}

	public function store(){
    $school = $this->input->post('school');
		$class = $this->input->post('class');
		$title = $this->input->post('title');
		$quantity = $this->input->post('quantity');
		$special_price = $this->input->post('special_price');
		$discount = $this->input->post('discount');
		$type = $this->input->post('type');
		if(empty($school)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a school']);
      exit();
		}
    if(empty($class)){
		  echo json_encode(['status'=>403, 'message'=>'Please select class']);
      exit();
		}
		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a title']);
		    exit();
		}
    if(empty($quantity)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a quantity']);
         exit();
	 }
   if(empty($special_price)){
    echo json_encode(['status'=>403, 'message'=>'Please enter a price']);
       exit();
  }

  // if(empty($type)){
  //   echo json_encode(['status'=>403, 'message'=>'Please enter a type']);
  //      exit();
  // }
    $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
    $config = array(
      'upload_path' 	=> 'uploads/items',
      'file_name' 	=> str_replace(' ','',$title).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/items/'.$config['file_name'].'.'.$type;
      }
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
    
    $uid = uniqid();
    $data = array(
      'sci_uid' => $uid,
      's_id'        => $school,
      'sc_id'       => $class,
      'title'     => $title,
      'quantity'        => $quantity,
      'special_price'       => $special_price,
      'discount'       => $discount,
      //'type'       => $type,
      'image'       => $image,
      'create_date' => date('d-m-Y H:i:s'),
    );
    $checkbookname = $this->Class_item_model->check_class_items($school,$class,$title);

    if($checkbookname){
      echo json_encode(['status'=>403,'message'=>'Class Item already exists']);
      exit();
    }

    $store = $this->Class_item_model->store_class_items($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Add items successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
  
	}

  public function editForm(){
    $uid = $this->input->post('uid');
		$schools = $this->School_model->get_all_schools();
    $item = $this->Class_item_model->get_class_item(array('sci_uid'=>$uid));
    $types = array('B','S','A');
    ?>
          <div class="form-group">
          <label for="school" class="col-form-label">School:</label>
            <select class="form-control" name="school" id="school" onchange="getClasses(this.value)">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>" <?= $item->s_id == $school->id ? 'selected' : '' ;?>><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
          <label for="class" class="col-form-label">Class:</label>
            <select class="form-control class" name="class" id="class" >
              <option value="<?=$item->sc_id?>"><?=$item->className?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Item Title:</label>
            <input  class="form-control" name="title" id="title" value="<?=$item->title?>"> 
          </div>
          <div class="form-group">
            <label for="quantity" class="col-form-label">Quantity:</label>
            <input  class="form-control" name="quantity" id="quantity" value="<?=$item->quantity?>"> 
          </div>
          <div class="form-group">
            <label for="special_price" class="col-form-label">Price:</label>
            <input  class="form-control" name="special_price" id="special_price" value="<?=$item->special_price?>"> 
          </div>
          <div class="form-group">
            <label for="discount" class="col-form-label">Discount:</label>
            <input  class="form-control" name="discount" id="discount" value="<?=$item->discount?>"> 
          </div>
          <!-- <div class="form-group">
            <label for="type" class="col-form-label">Type:</label>
            <select class="form-control" name="type" id="type" >
              <option value="">Select Type</option>
              <?php //foreach($types as $type){?>
                <option value="<?//=$type?>" <?//=$type==$item->type ? 'selected': '' ;?>><?//=$type?></option>
                <?php //} ?>
            </select>
          </div> -->
        
          <div class="form-group">
            <label for="image" class="col-form-label">Item Picture:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
          <div class="form-group">
            <img src="<?= base_url($item->image != "" ? $item->image : '') ;?>" width="100" height="100">
          </div>
          <input type="hidden" name="uid" id="uid" value="<?=$uid?>">
   
    <?php
  }

  public function update(){
    $uid = $this->input->post('uid');
    $item = $this->Class_item_model->get_class_item(array('sci_uid'=>$uid));
    $school = $this->input->post('school');
		$class = $this->input->post('class');
		$title = $this->input->post('title');
		$quantity = $this->input->post('quantity');
		$special_price = $this->input->post('special_price');
		$discount = $this->input->post('discount');
		// $type = $this->input->post('type');
		if(empty($school)){
		  echo json_encode(['status'=>403, 'message'=>'Please select a school']);
      exit();
		}
    if(empty($class)){
		  echo json_encode(['status'=>403, 'message'=>'Please select class']);
      exit();
		}
		if(empty($title)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a title']);
		    exit();
		}
    if(empty($quantity)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a quantity']);
         exit();
	 }
   if(empty($special_price)){
    echo json_encode(['status'=>403, 'message'=>'Please enter a price']);
       exit();
  }

  // if(empty($type)){
  //   echo json_encode(['status'=>403, 'message'=>'Please enter a type']);
  //      exit();
  // }
  $checkbookname = $this->Class_item_model->check_class_items($school,$class,$title,$uid);
  if($checkbookname){
    echo json_encode(['status'=>403,'message'=>'Class Item already exists']);
    exit();
  }
  //echo $_FILES['image']['name'];
    $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
    $config = array(
      'upload_path' 	=> 'uploads/items',
      'file_name' 	=> uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/items/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($item->image)){
       $image = $item->image;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }

    $data = array(
      's_id'          => $school,
      'sc_id'         => $class,
      'title'         => $title,
      'quantity'      => $quantity,
      'special_price' => $special_price,
      'discount'      => $discount,
      'image'         => $image,
      'modify_date'   => date('d-m-Y H:i:s'),
    );
// print_r($data);
// echo $uid; die;
     $update = $this->Class_item_model->update_class_items($data,array('sci_uid'=>$uid));
    if($update){
      echo json_encode(['status'=>200, 'message'=>'Update items successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function delete(){
    $uid = $this->input->post('uid');

        $itemdata =  array(
          'status' => 0
        );
       $item_status =  $this->$this->Class_item_model->update_class_items($itemdata,array('sci_uid',$uid));
  }	


  function fetch_user(){  
    $this->load->model("crud_model");  
    $fetch_data = $this->Class_item_model->get_class_item();  
    $data = array();  
    foreach($fetch_data as $row)  
    {  
         $sub_array = array();  
         $sub_array[] = '<img src="'.base_url().'upload/'.$row->image.'" class="img-thumbnail" width="50" height="35" />';  
         $sub_array[] = $row->title;  
         $sub_array[] = $row->special_price;  
         $sub_array[] = '<button type="button" name="update" id="'.$row->id.'" class="btn btn-warning btn-xs">Update</button>';  
         $sub_array[] = '<button type="button" name="delete" id="'.$row->id.'" class="btn btn-danger btn-xs">Delete</button>';  
         $data[] = $sub_array;  
    }  
    $output = array(  
         "draw"                    =>     intval($_POST["draw"]),  
         "recordsTotal"          =>      $this->Class_item_model->get_class_item(),  
         "recordsFiltered"     =>     $this->Class_item_model->get_class_item_byID(),  
         "data"                    =>     $data  
    );  
    echo json_encode($output);  
}  

public function importItems(){
  $uniqid = uniqid();
  $s_id = $this->input->post('school');
  $class = $this->input->post('class');
  $bookset_title = $this->input->post('booktitle');
  $school_image = $this->School_model->get_schoolImageById($s_id);
  //print_r($school_image);die;
  if (isset($_FILES["importFile"])) {
    $config['upload_path']   = "uploads/importItems/";
    $config['allowed_types'] = 'text/plain|text/csv|csv';
    $config['max_size']      = '2048';
    $config['file_name']     = $uniqid.$_FILES["importFile"]['name'];
    $config['overwrite']     = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload("importFile")) {
      echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
     exit();
    } else {
      $file_data = $this->upload->data();
      $file_path = 'uploads/importItems/'.$file_data['file_name'];
      if ($this->csvimport->get_array($file_path)) {
        $csv_array = $this->csvimport->get_array($file_path);
        // check if any duplicate record exist in system
        $duplicate_found = false;
        $err_msg = '';
        //print_r($csv_array);die;
        $insert_data =array();
        $bookset_price = 0;
        foreach ($csv_array as $key => $row) {
          $sci_uid = uniqid();
            $insert_data[] = array(
            'sci_uid' => $sci_uid,
            's_id' => $s_id,
            'sc_id' => $class,
            //'isbn' =>  '',
            'title' => $row['title'],
            'quantity' => $row['quantity'],
            'special_price' => $row['special_price'],
            'discount' => $row['discount'],
            'loose_discount' => $row['loose_discount'],
            'type' => $row['type'],
            'image' => $school_image->image,
            'create_date' => date('Y-m-d H:i:s'),
            'modify_date' => date('Y-m-d H:i:s'),
            'status' => 1
          );
          $bookset_price +=$row['special_price'];
           
          //$insert_id = $this->Class_item_model->store_class_items_import($insert_data);   
      }
      $insert_id = $this->Class_item_model->store_class_items_import($insert_data);
      $itemID = implode(',',$insert_id);
      $book_set_uid = uniqid();
      $book_set_data = array(
        'book_set_uid' => $book_set_uid,
        's_id' => $s_id,
        'sc_id' => $class,
        'items' => $itemID,
        'title' => $bookset_title,
        'price' => $bookset_price,
        'image' => $school_image->image,
      );
       
      $store_book_set = $this->Common_model->store_book_set($book_set_data);
        echo json_encode(['status'=>200, 'message'=>'Upload Items Successfully']);
        exit();
    
    }else{
      echo json_encode(['status'=>403, 'message'=>'Please Upload Correct CSV files']);
     exit();
    }
  } 
}else{
  echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
  exit();
}
}

}