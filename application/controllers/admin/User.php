<?php 
class User extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->logged_in();
		$this->load->model('user_model');
	}

	public function index()
	{	$data['page_title'] = 'User';
		$data['siteinfo'] = $this->siteinfo();
        $data['users'] = $this->user_model->get_all_users();
		$this->admin_template('users',$data);
	}

	public function update_status()
	{
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';
		

        $userdata =  array(
          'status' => $user_status
        );
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
	}

	public function viewUser(){
		$userid = $this->input->post('userid');
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
		?>
			<div class="box mb-0">
				<div class="box-body box-profile">            
					<div class="row">
						<div class="col-12">
							<h4 class="box-inverse p-2">Personal Information</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=$user->mobile;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=$user->address;?> <br> <?=ucfirst(strtolower($user->city));?>, <?=ucfirst(strtolower($user->state));?>, <?=$user->pin_code;?></span></p>
							</div>
							<br>
							<h4 class="box-inverse p-2">Shipping Address</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=!empty($user->shipping_name)?$user->shipping_name:$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=!empty($user->shipping_email)?$user->shipping_email:$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=!empty($user->shipping_mobile)?$user->shipping_mobile:$user->mobile;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=!empty($user->shipping_address)?$user->shipping_address:$user->address;?> <br> <?=!empty($user->shipping_street)?$user->shipping_street.',':'';?> <?=!empty($user->shipping_city)?ucfirst(strtolower($user->shipping_city)):ucfirst(strtolower($user->city));?>, <?=!empty($user->shipping_state)?ucfirst(strtolower($user->shipping_state)):ucfirst(strtolower($user->state));?>, <?=!empty($user->shipping_pin_code)?$user->shipping_pin_code:$user->pin_code;?></span></p>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		<?php
	  }
	
}