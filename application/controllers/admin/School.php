<?php 
class School extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_admin_logged_in();
    $this->logged_in();
		$this->load->model('School_model');
    $this->load->model('Common_model');
    $this->load->model('School_class_model');
    $this->load->model('Class_item_model');
    
	}

	public function index()
	{	
		$data['page_title'] = 'Schools';
    $data['siteinfo'] = $this->siteinfo();
		$data['schools'] = $this->School_model->get_all_schools();
		$data['states'] = $this->Common_model->get_states();
		$data['cities'] = $this->Common_model->get_cities();
    $this->admin_template('school',$data);
	}

	public function store(){
		$name = $this->input->post('name');
		$city = $this->input->post('city');
		$address = $this->input->post('address');
		$state = $this->input->post('state');
		//$name = $this->input->post('image');
		if(empty($name)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a school name']);
      exit();
		}
    if(empty($address)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a address']);
      exit();
		}
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a state']);
		    exit();
		}
    if(empty($city)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a city']);
         exit();
	}
    $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/school',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => '*',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/school/'.$config['file_name'].'.'.$type;
      }
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }
    
    $uid = uniqid();
    $data = array(
      'school_u_id' => $uid,
      'name'        => $name,
      'image'       => $image,
      'address'     => $address,
      'city'        => $city,
      'state'       => $state,
      'create_date' => date('d-m-Y H:i:s'),
    );
    $schoolcheck= $this->School_model->school_name_check($name);
    if($schoolcheck){
      echo json_encode(['status'=>403,'message'=>'School already exists']);
      exit();
    }
    $store = $this->School_model->store_school($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Add school successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}
  public function importSchools(){
    $uniqid = uniqid();
			$s_id = $this->input->post('s_id');
			$language_id = $this->input->post('language_id');
			if (isset($_FILES["importFile"])) {
			  $config['upload_path']   = "uploads/importClasses/";
			  $config['allowed_types'] = 'text/plain|text/csv|csv';
			  $config['max_size']      = '2048';
			  $config['file_name']     = $uniqid.$_FILES["importFile"]['name'];
			  $config['overwrite']     = TRUE;
			  $this->load->library('upload', $config);
			  $this->upload->initialize($config);
			  if (!$this->upload->do_upload("importFile")) {
				echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			   exit();
			  } else {
				$file_data = $this->upload->data();
				$file_path = 'uploads/importClasses/'.$file_data['file_name'];
				if ($this->csvimport->get_array($file_path)) {
				  $csv_array = $this->csvimport->get_array($file_path);
				  // check if any duplicate record exist in system
				  $duplicate_found = false;
				  $err_msg = '';
				  //print_r($csv_array);die;
				  foreach ($csv_array as $key => $row) {
					$s_u_id = uniqid();
					  $insert_data = array(
              'school_u_id' => $s_u_id,
              'name'        => $row['SchoolName'],
              'image'       => 'public/website/images/dummy_image.jpg',
              'address'     => $row['address'],
              'city'        => $row['city'],
              'state'       => $row['state'],
              'create_date' => date('d-m-Y H:i:s'),
						
					);
					$insert_id = $this->School_model->store_school($insert_data);  
				} 
				  echo json_encode(['status'=>200, 'message'=>'Upload Items Successfully']);
				  exit();
			  
			  }else{
				echo json_encode(['status'=>403, 'message'=>'Please Upload Correct CSV files']);
			   exit();
			  }
			} 
		  }else{
			echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
			exit();
		  }
  }

  public function editForm(){
    $uid = $this->input->post('uid');
    $school = $this->School_model->get_school(array('school_u_id'=>$uid));
    $states = $this->Common_model->get_states();
    ?>
          <div class="form-group">
            <label for="name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" name="name" id="name" value="<?=$school->name ?>">
          </div>
          <div class="form-group">
            <label for="state" class="col-form-label">Address:</label>
            <textarea  class="form-control" name="address" id="address"><?=$school->address ?> </textarea>
          </div>
          <div class="form-group">
            <label for="state" class="col-form-label">State:</label>
            <select class="form-control" name="state" id="state" onchange="getCity(this.value )">
              <option value="">Select State</option>
              <?php foreach($states as $state){?>
                <option value="<?=$state->id?>" <?=$school->state == $state->id ? 'selected' : '' ;?>><?=$state->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="city" class="col-form-label">District:</label>
            <select class="form-control city" name="city" id="citys">
              <option value="<?=$school->id ?>"><?=$school->cities_name ?></option>
            </select>
          </div>
        
          <div class="form-group">
            <label for="image" class="col-form-label">School Picture:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
          <div class="form-group">
            <img src="<?=base_url($school->image != "" ? $school->image : '') ;?>" width="100" height="100">
          </div>
          <input type="hidden" name="uid" id="uid" value="<?=$uid?>">
   
    <?php
  }

  public function update(){
    $uid = $this->input->post('uid');
    $school = $this->School_model->get_school(array('school_u_id'=>$uid));
		$name = $this->input->post('name');
		$city = $this->input->post('city');
		$address = $this->input->post('address');
		$state = $this->input->post('state');
		//$image = $this->input->post('image');
		if(empty($name)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a school name']);
      exit();
		}
    if(empty($address)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a address']);
      exit();
		}
		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please enter a state']);
		    exit();
		}
    if(empty($city)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a city']);
         exit();
	}
  //echo($_FILES['image']['name']);die();
    $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
    $config = array(
      'upload_path' 	=> 'uploads/school',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/school/'.$config['file_name'].'.'.$type;
        //echo $image;die();
      }
    }elseif(!empty($school->image)){
      $image = $school->image;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }

    $data = array(
      'name'        => $name,
      'image'       => $image,
      'address'     => $address,
      'city'        => $city,
      'state'       => $state,
    );
    $schoolcheck= $this->School_model->school_name_check($name,$uid);
    if($schoolcheck){
      echo json_encode(['status'=>403,'message'=>'School already exists']);
      exit();
    }
    $update = $this->School_model->update_school($data,array('school_u_id'=>$uid));
    if($update){
      echo json_encode(['status'=>200, 'message'=>'Update school successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function delete(){
    $uid = $this->input->post('uid');
    $school = $this->School_model->get_school(array('school_u_id'=>$uid));
    $data =  array(
      'status' => 0
    );

       $school_status = $this->School_model->update_school($data,array('school_u_id'=>$uid));
    
    if($school_status){
      $classdata =  array(
        'status' => 0
      );
       $class_status = $this->School_class_model->update_class($classdata,array('s_id',$school->id));

      if($class_status){
        $itemdata =  array(
          'status' => 0
        );
       $item_status =  $this->Class_item_model->update_class_items($itemdata,array('s_id'=>$school->id));
      }
    }


  }


	
}