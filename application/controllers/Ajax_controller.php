<?php 
class Ajax_controller extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();
		$this->load->model('School_model');
    $this->load->model('Common_model');
    $this->load->model('School_class_model');
	}

	public function get_city()
    {
       $stateID =  $this->input->post('stateID');
       $cities = $this->Common_model->get_state_wise_city($stateID);
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>"><?=$city->city?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }

    public function get_class(){
      $schoolID =  $this->input->post('schoolID');
      $classes = $this->School_class_model->get_class(array('s_id'=>$schoolID, 'status' => '1'));
      
      if(count($classes) > 0)
      {
        ?>
        <option value="">Select Class</option>
        <?php foreach($classes as $class){ ?>
          <option value="<?=$class->id?>"><?=$class->class?></option>
        <?php
        }
      }else
      {
        ?>
        <option value="">No Class found</option>
        <?php
      }
    }
}