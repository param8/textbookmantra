<?php 

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	
	}
}

class Admin_Controller extends MY_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();
       $this->load->model('Wishlist_model');
	   $this->load->model('Common_model');
	   $this->load->model('School_model');

// 		if(empty($this->session->userdata('logged_in'))) {
// 			$session_data = array('logged_in' => FALSE);
// 			$this->session->set_userdata($session_data);
// 		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == TRUE) {
			if($session_data['type'] == 'Admin')
			{
			// redirect('admin/dashboard', 'refresh');	
			}else{
			    if($session_data['type'] != 'Admin'){
			        redirect('home', 'refresh');
			    }else{
			       // redirect('admin/dashboard', 'refresh');	
			    }
				
			}	
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('home', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('login', 'refresh');
		}
	}

	public function totalwishlist(){
		if($this->session->userdata()){
			$wishlist = $this->Wishlist_model->getwishlist();
		  return  $totalwishlist = count($wishlist);
		}else{
		return	$totalwishlist = 0;
		}
	}

	 public function siteinfo(){
		$siteinfo = $this->Common_model->get_site_info();
	  return $siteinfo;
    
	 }

	 public function get_states(){
		$get_states = $this->Common_model->get_states();
	  return $get_states;
	 }
    
	 public function aboutinfo(){
		$about_info = $this->Common_model->get_about();
		return $about_info;
	 }


	 public function schools(){
	 return	$schools = $this->School_model->get_all_schools();
	 }
	 public function school_detail($data = array()){
      return $this->School_model->get_school($data);
	 }


	 public function admin_template($page = null, $data = array())
	 {	$data['siteinfo'] = $this->siteinfo();
		$this->load->view('admin/layout/head',$data);
		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/'.$page);
		$this->load->view('admin/layout/footer');
	 }

	 public function website_template($page = null, $data = array())
	 {
		$data['siteinfo'] = $this->siteinfo();
		$data['states']= $this->get_states();
		$this->load->view('layout/head',$data);
		$this->load->view('layout/header');
		$this->load->view($page);
		$this->load->view('layout/school_category');
		$this->load->view('layout/footer');
	 }

}