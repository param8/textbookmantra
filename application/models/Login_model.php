<?php 



class Login_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}

	/* 

		This function checks if the email and password matches with the database

	*/

	public function login($email, $password) {
		if($email && $password) {
			$sql = "SELECT * FROM users WHERE email = ? OR mobile = ?";
			$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();
				$encryptPass=$password;

				if($encryptPass==$result['password'])
				{
          $session = array(
              'u_id'     => $result['u_id'],
              'name'     => $result['name'],
              'email'    => $result['email'],
              'mobile'   => $result['mobile'],
              'address'  => $result['address'],
              'pin_code' => $result['pin_code'],
              'city'     => $result['city'],
              'state'    => $result['state'],
              'type'     => $result['type'],
              'status'   => $result['status']
          );
          $this->session->set_userdata($session);
					return $result;	
				}
				else {
					return false;
				}
			}else {
				return false;
			}
		}
	}

}