<?php 

class Common_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_states()
	{
	    $this->db->order_by('name','ASC');
		return $this->db->get('states')->result();

	}

    public function get_cities()
	{
		return $this->db->get('cities')->result();
	}

  public function get_pincode()
	{
          $this->db->where('status',1);
		return $this->db->get('pin_code')->result();

	}

	public function get_state_wise_city($stateID){
		$this->db->where('state_id',$stateID);
		return $this->db->get('cities')->result();
	}

	public function get_all_book_set(){
		$this->db->select('book_set.*, school_details.name as schoolName, school_class.class as className'); 
		$this->db->join('school_details', 'book_set.s_id = school_details.id');
        $this->db->join('school_class', 'school_class.id = book_set.sc_id');
        $this->db->where('book_set.status', 1);
	    $this->db->order_by('book_set.id','desc');
		return $this->db->get('book_set')->result();
	}

	public function get_book_sets($data){
		$this->db->select('book_set.*, school_details.name as schoolName, school_class.class as className'); 
		$this->db->join('school_details', 'book_set.s_id = school_details.id');
        $this->db->join('school_class', 'school_class.id = book_set.sc_id');
        $this->db->where('book_set.status', 1);
		$this->db->where($data);
		return $this->db->get('book_set')->result();
	}

	public function get_loose_books($data){
		$this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
		if($this->session->userdata('search')){
			$this->db->like('title', $this->session->userdata('search'));
		}
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
		$this->db->where($data);
		return $this->db->get('school_class_items')->result(); 
		
	}

	public function get_book_set($data){
		$this->db->select('book_set.*, school_details.name as schoolName, school_class.class as className'); 
		$this->db->join('school_details', 'book_set.s_id = school_details.id');
        $this->db->join('school_class', 'school_class.id = book_set.sc_id');
        $this->db->where('book_set.status', 1);
		$this->db->where($data);
		return $this->db->get('book_set')->row();
	}
   
	public function store_book_set($data){
		return $this->db->insert('book_set',$data);
	}

	public function update_book_set($data,$id){
		$this->db->where($id);
		return $this->db->update('book_set',$data);
	}

	public function check_book_set_title($title,$school,$class,$bookSetID = null){
		$this->db->where('book_set_uid !=',$bookSetID);
		$this->db->where('title',$title);
		$this->db->where('s_id',$school);
		$this->db->where('sc_id',$class);
		$query = $this->db->get('book_set');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_site_info(){
   return $this->db->get('site_info')->row();
	}

	public function store_siteInfo($data){
		$this->db->where('id',1);
	return $this->db->update('site_info',$data);
	}

   public function store_contact($data) {
	return $this->db->insert('contact', $data);
	}
	
   	public function get_about(){
		return $this->db->get('about_us')->row();
   	}
	
	public function store_aboutus($data){
		$this->db->where('id',1);
		return $this->db->update('about_us',$data);
	}

	public function get_all_enquiry(){
		return $this->db->get('contact')->result();
	}

	public function get_dashboardData($type){
		if($type == 'school'){		
			$this->db->where('status',1);	
			$query = $this->db->get('school_details');	
			return $query->num_rows();		
		}elseif($type == 'class'){	
			$this->db->where('status',1);			
			$query = $this->db->get('school_class');
			//echo $this->db->last_query();die;	
			return $query->num_rows();		
		}elseif($type == 'items'){	
			$this->db->where('status',1);			
			$query = $this->db->get('school_class_items');	
			return $query->num_rows();		
		}elseif($type == 'bookSet'){	
			$this->db->where('status',1);			
			$query = $this->db->get('book_set');	
			return $query->num_rows();		
		}
		
	
	}


}