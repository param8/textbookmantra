<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_all_users()
	{
		$this->db->where('type', 'User');
	    $this->db->order_by('id','desc');
		return $this->db->get('users')->result();
	}

	public function get_user($data){
		$this->db->where($data);
		$query =  $this->db->get('users');
		return $query;
	}

	public function update_user_status($data,$id)
	{
		$this->db->where($id);
		return $this->db->update('users',$data);
	}

	public function get_user_details($data){
		$this->db->select('users.*, shipping_details.name as shipping_name, shipping_details.mobile as shipping_mobile,
		 shipping_details.address as shipping_address, shipping_details.street as shipping_street,shipping_details.pin_code as shipping_pin_code,
		 shipping_details.city as shipping_city, shipping_details.state as shipping_state'); 
		$this->db->join('shipping_details', 'users.id = shipping_details.uid');
		$this->db->where($data);
		$result =  $this->db->get('users');
		if($result->num_rows() > 0)
		{
			return $result->row();	
		}else{
			$this->db->where($data);
			return $this->db->get('users')->row();	
		}
	}
	

}