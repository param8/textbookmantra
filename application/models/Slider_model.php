<?php 

class Slider_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_all_slider()
	{   
        $this->db->select('*');
	    $this->db->order_by('id','desc');
		$this->db->where('status',1);
		return $this->db->get('slider')->result();
    	
	}

	public function add_slider($data)
	{
		return $this->db->insert('slider',$data);
	}
	
	public function get_slider()
	{
    $this->db->select('*');
    $this->db->from('slider');
    //$this->db->where($data);
		return $this->db->get()->result();	
	}
	
	public function get_sliderAdmin($data)
	{
    $this->db->select('*');
    $this->db->from('slider');
    $this->db->where($data);
		return $this->db->get()->result();	
	}

	public function update_slider($data,$uid){
		$this->db->where('slider_uid',$uid);
		return $this->db->update('slider',$data);
		//echo $this->db->last_query();die;
	}

	public function delete_slider($uid){
		$this->db->where('slider_uid',$uid);
		return $this->db->delete('slider');
	}

	public function update_slider_status($data,$id)
	  {
		  $this->db->where($id);
		  return $this->db->update('slider',$data);
	  }
}