<?php 



class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}

	/* 

		This function checks if the email and password matches with the database

	*/
 
    public function login($email, $password,$type) {
		if($email && $password) {
      if($type == 'Admin'){
			  $query = $this->db->query("SELECT * FROM users WHERE (email = '$email' OR mobile = '$email') AND `type` = '$type'");
      }else{
        $query = $this->db->query("SELECT * FROM users WHERE email = '$email' OR mobile = '$email'");
      }
			//$query = $this->db->query($sql, array($email,$email));
			if($query->num_rows() == 1) {
				$result = $query->row_array();
				$encryptPass=md5($password);

				if($encryptPass==$result['password'])
				{
          $session = array(
            'id'       => $result['id'],
            'u_id'     => $result['u_id'],
            'name'     => $result['name'],
            'email'    => $result['email'],
            'mobile'   => $result['mobile'],
            'address'  => $result['address'],
            'pin_code' => $result['pin_code'],
            'city'     => $result['city'],
            'state'    => $result['state'],
            'type'     => $result['type'],
            'status'   => $result['status'],
			'password'   => $result['password'],
            'logged_in'=> TRUE
          );
        $this->session->set_userdata($session);
				return $result;	
			}
			else {
				return false;
			}
			}else {
				return false;
			}
		}
	}


	public function payment_login($id) {
    $query = $this->db->query("SELECT * FROM users WHERE id = $id");
		$result = $query->row_array();
          $session = array(
            'id'       => $result['id'],
            'u_id'     => $result['u_id'],
            'name'     => $result['name'],
            'email'    => $result['email'],
            'mobile'   => $result['mobile'],
            'address'  => $result['address'],
            'pin_code' => $result['pin_code'],
            'city'     => $result['city'],
            'state'    => $result['state'],
            'type'     => $result['type'],
            'status'   => $result['status'],
            'logged_in'=> TRUE
          );
        $this->session->set_userdata($session);
				return $result;	
	}

  public function register($data) {
	  return $this->db->insert('users', $data);
   }
	  public function get_all_classitem()
	  {
		  $this->db->where('status', 1);
		  $this->db->order_by('id','desc');
		  return $this->db->get('school_class_items')->result();
	  }

}