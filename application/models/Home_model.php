<?php 

class School_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_home_detail()
	{
		$this->db->where('status', 1);
	    $this->db->order_by('id','desc');
		return $this->db->get('school_details')->result();
    	
	}


	

}