<?php 

class Language_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_all_languages()
	{
		$this->db->where('status', 1);
	    $this->db->order_by('id','asc');
		return $this->db->get('languages')->result();	
	}
	
	public function is_language_exist($lang_name, $uid=null)
	{
		$this->db->where('language_name',$lang_name);
		if($uid != null)
		{
			$this->db->where('lang_uid !=',$uid);
		}
		$query = $this->db->get('languages');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function add_language($data)
	{
		return $this->db->insert('languages',$data);
	}

	public function update_language($data, $uid)
	{
		$this->db->where('lang_uid', $uid);
        return $this->db->update('languages', $data);
	}

}