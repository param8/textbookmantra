<?php 

class Class_item_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_all_class_item($limit=null,$page=null)
	{   
        $this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
		if($this->session->userdata('search')){
			$this->db->like('title', $this->session->userdata('search'));
		}
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
	    $this->db->order_by('school_class_items.id','desc');
		$this->db->limit($limit,$page);
		return $this->db->get('school_class_items')->result();  
		// echo $this->db->last_query();die;
		
	}

	public function get_all_loosebook($limit=null,$page=null)
	{   
        $this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
		if($this->session->userdata('search')){
			$this->db->like('title', $this->session->userdata('search'));
		}
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
		$this->db->where('school_class_items.type', 'B');
	    $this->db->order_by('school_class_items.id','desc');
		$this->db->limit($limit,$page);
		return $this->db->get('school_class_items')->result();  
		// echo $this->db->last_query();die;
		
	}
	public function get_all_stationary($limit=null,$page=null)
	{   
        $this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
		if($this->session->userdata('search')){
			$this->db->like('title', $this->session->userdata('search'));
		}
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
		$this->db->where('school_class_items.type', 'S');
	    $this->db->order_by('school_class_items.id','desc');
		$this->db->limit($limit,$page);
		return $this->db->get('school_class_items')->result();  
		// echo $this->db->last_query();die;
		
	}

	public function get_count_lossebook(){
		$this->db->select('school_class_items.*'); 
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
        $this->db->where('school_class_items.type', 'B');
	    $this->db->order_by('school_class_items.id','desc');
		$query =  $this->db->get('school_class_items')->result();
		return count($query);
	}
	public function get_count_stationary(){
		$this->db->select('school_class_items.*'); 
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where('school_class_items.status', 1);
        $this->db->where('school_class_items.type', 'S');
	    $this->db->order_by('school_class_items.id','desc');
		$query =  $this->db->get('school_class_items')->result();
		return count($query);
	}

	public function get_school_class_wise_item($data)
	{   
        $this->db->where($data);
		$query =  $this->db->get('school_class_items')->result();
		return $query;	
	}

	public function get_class_item($data){
		$this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
		$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
		$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
        $this->db->where($data);
		return $this->db->get('school_class_items')->row(); 
		//echo $this->db->last_query();die;	 	
	}

	public function get_class_item_byID($items){
    $query =array();
		foreach($items as $item){
			$this->db->select('school_class_items.*, school_class.class as className , school_details.name as schoolName'); 
			$this->db->join('school_class', 'school_class.id = school_class_items.sc_id','left');
			$this->db->join('school_details', 'school_details.id = school_class_items.s_id','left');
			$this->db->where('school_class_items.id',$item);
			$query[] =  $this->db->get('school_class_items')->row_array(); 
		}
    return $query;
		//echo $this->db->last_query();die;	 	
	}

	public function is_class_exist($sid, $lid, $cname)
	{
		$this->db->where('s_id',$sid);
		$this->db->where('language_id',$lid);
		$this->db->where('class',$cname);
		$query = $this->db->get('school_class');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function store_class_items($data)
	{
		return $this->db->insert('school_class_items',$data);
	}

	public function store_class_items_import($datas)
	{
		$insertedID = array();
		foreach($datas as $key=>$data){
			$this->db->insert('school_class_items',$data);
			$insertedID[] = $this->db->insert_id();
		}
		
		return $insertedID;
		 
	}

	public function update_class_items($data,$id)
	{
		$this->db->where($id);
		return $this->db->update('school_class_items',$data);
		// $this->db->update('school_class_items',$data);
		// echo $this->db->last_query();die();
		
	}	

	public function check_class_items($sid, $cid, $title,$uid=null)
	{
		$this->db->where('sci_uid !=',$uid);
		$this->db->where('s_id',$sid);
		$this->db->where('sc_id',$cid);
		$this->db->where('title',$title);
		$query = $this->db->get('school_class_items');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

}