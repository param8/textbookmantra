<?php 

class Cart_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encryption');

    }

    public function get_product($productID) {
      
		 $this->db->where('id', $productID);
         return $this->db->get('book_set')->row_array();
    }

    public function get_books($productID) {
      
      $this->db->where('id', $productID);
          return $this->db->get('school_class_items')->row_array();
     }

    public function get_shipping_address(){
        $this->db->select('shipping_details.*,cities.city as cityName,states.name as stateName,pin_code.pin_code as pinCode');
        $this->db->from('shipping_details');
        $this->db->join('pin_code','pin_code.id=shipping_details.pin_code','left');
        $this->db->join('cities','cities.id=shipping_details.city','left');
        $this->db->join('states','states.id=shipping_details.state','left');
        $this->db->where('shipping_details.status',1);
        $this->db->where('shipping_details.uid',$this->session->userdata('id'));
        return $this->db->get()->result();
      }

      public function get_billing_address($condition){
        $this->db->select('shipping_details.*,cities.city as cityName,states.name as stateName,pin_code.pin_code as pinCode');
        $this->db->from('shipping_details');
        $this->db->join('pin_code','pin_code.id=shipping_details.pin_code','left');
        $this->db->join('cities','cities.id=shipping_details.city','left');
        $this->db->join('states','states.id=shipping_details.state','left');
        $this->db->where($condition);
      
        return $this->db->get()->row();
      }

      public function update_address($data,$id)
      {
        $this->db->where('id',$id);
       return $this->db->update('shipping_details',$data);
      }

      public function store_shippingAddress($data){
        return $this->db->insert('shipping_details',$data);
      }

      public function store_order($data){
        $this->db->insert('orders',$data);
        return $this->db->insert_id();
      }

      public function store_item($item_order){
       return $this->db->insert('order_items',$item_order);
      }

      public function update_order($data,$condition){
        $this->db->where($condition);
        return $this->db->update('orders',$data);
      }

      public function get_order($condition){
        $this->db->where($condition);
        return $this->db->get('orders')->row();
      }

   

}