<?php 
class Product_model extends CI_Model {
  public function get_products(){
    $this->db->limit(10);
  return $this->db->get('school_details')->result();

  }

  public function get_product($data){
    $this->db->where($data);
  return $this->db->get('book_set')->row();

  }

  public function get_items($items){
    $items = explode(',',$items);
    $query = array();
    foreach($items as $item){
      $this->db->where('id',$item);
      $query[]=$this->db->get('school_class_items')->row_array();
    }
    return $query;
  }
public function get_schoolname($id){
  $this->db->where('id',$id);
  return $this->db->get('school_name')->row();

}
 
}
?>