<?php 

class School_class_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}



	public function get_all_school_class()
	{   
        $this->db->select('school_class.*, school_details.name, languages.language_name'); 
		$this->db->join('school_details', 'school_class.s_id = school_details.id');
        $this->db->join('languages', 'school_class.language = languages.id');
        //$this->db->where('school_class.status', 1);
	    $this->db->order_by('school_class.id','desc');
		return $this->db->get('school_class')->result();	
	}

	public function get_class($data){
		$this->db->where($data);
		//$this->db->where('s_id',$data);
		$this->db->where('school_class.status', 1);
		return $this->db->get('school_class')->result();
	}

	public function get_class_byID($data){
		$this->db->where($data);
		return $this->db->get('school_class')->row();
	}

	public function is_class_exist($sid, $lid, $cname, $uid=null)
	{
		$this->db->where('s_id',$sid);
		$this->db->where('language',$lid);
		$this->db->where('class',$cname);
		if($uid!=null)
		{
			$this->db->where('sc_u_id !=',$uid);
		}
		$query = $this->db->get('school_class');
		
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function store_class($data)
	{
		return $this->db->insert('school_class',$data);
	}

	public function update_class($data,$id){
		$this->db->where($id);
		return $this->db->update('school_class',$data);
	}
	
	public function get_school_class($data)
	{
    $this->db->select('*');
    $this->db->from('school_class');
    $this->db->where($data);
		return $this->db->get()->row();	
	}

	public function delete_school_class($uid){
		$data = array('status' => 2);
		$this->db->where('sc_u_id',$uid);
		return $this->db->update('school_class',$data);
	}
}