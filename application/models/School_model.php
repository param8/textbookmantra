<?php 
class School_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}

	public function get_all_schools()
	{
		$this->db->select('school_details.*, states.name as state_name, cities.city as cities_name');
    $this->db->from('school_details');
    $this->db->join('states', 'states.id = school_details.state','left');
    $this->db->join('cities', 'cities.id = school_details.city','left');
		$this->db->where('school_details.status', 1);
	  $this->db->order_by('school_details.id','desc');
		return $this->db->get()->result();	
	}

	public function get_school($data)
	{
    $this->db->select('school_details.*, states.name as state_name, cities.city as cities_name');
    $this->db->from('school_details');
    $this->db->join('states', 'states.id = school_details.state','left');
    $this->db->join('cities', 'cities.id = school_details.city','left');
    $this->db->where($data);
		return $this->db->get()->row();	
	}
	// public function school_detail($data = array()){
	// 	return $this->School_model->get_school($data);
	//    }
    public function get_schoolImageById($id)
	{
		$this->db->select('image');
		$this->db->from('school_details');
		$this->db->where('id',$id);
		return $this->db->get()->row();
	}

  public function store_school($data)
	{
		$query = $this->db->insert('school_details',$data);	
    return $query;
	}

  public function update_school($data,$id){
    $this->db->where($id);
    return $this->db->update('school_details',$data);
    //echo $this->db->last_query();die;
  }

public function school_name_check($name, $uid=null)
{
  $this->db->where('name',$name);
  $this->db->where('school_u_id !=',$uid);
  $query = $this->db->get('school_details');
  if($query->num_rows()>0){
    return true;
}else{
	return false;
}

}	

}