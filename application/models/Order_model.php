<?php 

class Order_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}

    
	public function get_all_orders($wh=null)
	{
		$this->db->select('orders.*, users.name'); 
		$this->db->join('users', 'orders.user_id = users.id');
	    $this->db->order_by('orders.created_at','desc');
		if($wh != null){
			$this->db->where($wh);	
		}
		//$this->db->join('school_class', 'orders.scid = school_class.id');
		if($this->session->userdata('school')){
			$this->db->where('school_class.s_id',$this->session->userdata('school'));
		}
		if($this->session->userdata('fromDate')){
			if($this->session->userdata('toDate')){
				$this->db->where('date(orders.created_at) >=', date('Y-m-d',strtotime($this->session->userdata('fromDate'))));
			    $this->db->where('date(orders.created_at) <=', date('Y-m-d',strtotime($this->session->userdata('toDate'))));
			}
		}
	    $this->db->order_by('orders.id','desc');
		return $this->db->get('orders')->result();	
		//echo $this->db->last_query();die;
	}

    public function get_order_details($order_id)
	{
		$this->db->select('orders.*, orders.id as order_id, users.*, users.id as user_id'); 
		$this->db->join('users', 'orders.user_id = users.id');
	    $this->db->where($order_id);
        return $this->db->get('orders')->row();	
	}
	
	public function get_order_items($wh)
	{
		$this->db->select('order_items.*, book_set.*, school_details.name as school_name, school_class.class as school_class');
		$this->db->join('book_set', 'order_items.book_set_id = book_set.id');
		$this->db->join('school_details', 'book_set.s_id = school_details.id'); 
		$this->db->join('school_class', 'book_set.sc_id = school_class.id'); 
		$this->db->where($wh);
	    $this->db->order_by('order_items.id','asc');
		return $this->db->get('order_items')->result();
	}
	
	public function get_shipping_details($wh)
	{
		$this->db->select('payment_detail'); 
		$this->db->where($wh);
	    return $this->db->get('orders')->row();
	}

	public function update_order($data, $order_id)
	{
		$this->db->where('id', $order_id);
        return $this->db->update('orders', $data);
	}
	
	public function delete_order($order_id){
		$this->db->where('id', $order_id);
		return $this->db->delete('orders');
	}
	
}