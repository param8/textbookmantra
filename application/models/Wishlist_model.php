<?php 
class Wishlist_model extends CI_Model 
{

public function __construct()
{
    parent::__construct();

}


public function getwishlist(){
    $this->db->select('wishlist.*,book_set.title,book_set.id as bookSetID ,book_set.image,book_set.price');
    $this->db->from('wishlist');
    $this->db->join('book_set', 'wishlist.product_id=book_set.id', 'left');
    $this->db->where('wishlist.user_id',$this->session->userdata('id'));
    return $this->db->get()->result();
    // echo $this->db->last_query();die;
}

public function store_wishList($data){
    return $this->db->insert('wishlist',$data);
}
public function get_products(){
    $this->db->limit(10);
  return $this->db->get('book_set')->result();
}

public function delete_wishList($id){
    $this->db->where('id', $id);
    return $this->db->delete('wishlist');
     //echo $this->db->last_query();die;
}

}